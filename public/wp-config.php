<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'wordpress');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@5;7%+kf:wCh?fZF[*zsh/r(IGC,g7jZlH=F$HX6X`H6u?0^VY4{b0I$4+uU3miL');
define('SECURE_AUTH_KEY',  '_Z+(#Z8Ik>W`]x]Z&- q_F~2D-en(l&@^Cqw8NiHN-.7Kp.09Ql#vo;:)r.++EW8');
define('LOGGED_IN_KEY',    'BUyfIE1a*Q0R19pQT=^IIOgT*Ao>tWLpHR@_W4_eJ1J*Ofb7aRqZX~)8~@WcOzTC');
define('NONCE_KEY',        'W|F$fUX5lZa![HT~]#<$lMZ4(.P+(LW|i=2vdd}9Le,PFJG_n7R}eE,9^J#lgrCq');
define('AUTH_SALT',        'qE){v8flb68&h&3:yT0:hL(|Ibd giuraj.{-O,(tWXseY@BSvGUdRC~ruTj5:#5');
define('SECURE_AUTH_SALT', '0Dd?ni2X~CY*4z#8xL+vg3fw`2!];tsx]D<@tRYX3 QERRQ@&mCWE`A*c*Z?Bk^O');
define('LOGGED_IN_SALT',   '|h81(Uth#M,46=)T=t4Y*_KpNs|z<y8FyN=W9J&*U.y6_2<3<Tbrd*f~`,j&S_-p');
define('NONCE_SALT',       ';o3.??v-;bv@>CSJ1uDrbw# FuZfxs^Mncm=#vka]a46 b&ET0)=fsflA_.r$*)V');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
