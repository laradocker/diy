function RasterJob(width, height) {
    this.width = width, this.height = height, this.instructions = [], this.imageIds = [], this.imageIdIndexLookup = {}, this.pushInstructions = function() {
        for (var i = 0; i < arguments.length; i++) this.instructions.push(arguments[i])
    }, this.clear = function() {
        this.pushInstructions(RasterInstructionSymbols.clear)
    }, this.rect = function(x, y, width, height, colour) {
        this.pushInstructions(RasterInstructionSymbols.rect, colour, this.round(x), this.round(y), this.round(width), this.round(height))
    }, this.fillRect = function(x, y, width, height, colour) {
        this.pushInstructions(RasterInstructionSymbols.fillRect, colour, this.round(x), this.round(y), this.round(width), this.round(height))
    }, this.fillText = function(x, y, text, font, colour, rotationRadians) {
        this.pushInstructions(RasterInstructionSymbols.fillText, rotationRadians, colour, font, text, this.round(x), this.round(y))
    }, this.drawClippedImage = function(x, y, width, height, clipX, clipY, clipWidth, clipHeight, imageId) {
        this.pushInstructions(RasterInstructionSymbols.drawClippedImage, this.indexifyImageId(imageId), this.round(clipX), this.round(clipY), this.round(clipWidth), this.round(clipHeight), this.round(x), this.round(y), this.round(width), this.round(height))
    }, this.drawImage = function(x, y, width, height, imageId) {
        this.pushInstructions(RasterInstructionSymbols.drawImage, this.indexifyImageId(imageId), this.round(x), this.round(y), this.round(width), this.round(height))
    }, this.buildCompactString = function() {
        var compactString = "";
        compactString += "v1.1$", compactString += this.width + "|" + this.height + "$";
        for (var seperator = "", i = 0; i < this.imageIds.length; i++) compactString += seperator + this.imageIds[i], seperator = "|";
        compactString += "$";
        for (var i = 0; i < this.instructions.length; i++) compactString += seperator + this.instructions[i], seperator = "|";
        return compactString
    }, this.indexifyImageId = function(imageId) {
        return null == this.imageIdIndexLookup[imageId] && (this.imageIds.push(imageId), this.imageIdIndexLookup[imageId] = this.imageIds.length - 1), this.imageIdIndexLookup[imageId]
    }, this.round = function(f) {
        return Math.round(10 * f) / 10
    }
}
siteApp.controller("ConfiguratorController", ["$scope", "$rootScope", "$http", "$window", "$interval", "$sce", "dataUrlBuilder", "validator", "doorPanelHelper", "materialHelper", "doorSelectionSanitiser", "doorQuantityRulesCalculator", "apertureSizeCalculator", "physicalMetricsCalculator", "renderMetricsCalculator", "doorPlacementPlanManager", "doorPanelPlacementConstraintsBuilder", "previewRenderer", "previewDoorsRenderer", "previewSettings", "previewHotSpotManager", "previewRasteriser", "renderingImageManager", "doorPanelConfigurationSelectionChecker", "openingSizeRulesHelper", "doorLinerQuantityCalculator", "price", "priceCalculator", "staticData", "inputData", "calculatedData", "customerDetails", "installationDetails", "submissionDetails", "tabData", "settings", "domManager", "submitter", "scl", "translator", "uiHelper", "util", function($scope, $rootScope, $http, $window, $interval, $sce, dataUrlBuilder, validator, doorPanelHelper, materialHelper, doorSelectionSanitiser, doorQuantityRulesCalculator, apertureSizeCalculator, physicalMetricsCalculator, renderMetricsCalculator, doorPlacementPlanManager, doorPanelPlacementConstraintsBuilder, previewRenderer, previewDoorsRenderer, previewSettings, previewHotSpotManager, previewRasteriser, renderingImageManager, doorPanelConfigurationSelectionChecker, openingSizeRulesHelper, doorLinerQuantityCalculator, price, priceCalculator, staticData, inputData, calculatedData, customerDetails, installationDetails, submissionDetails, tabData, settings, domManager, submitter, scl, translator, uiHelper, util) {
    $scope.subView = "", $scope.forms = {
        customerDetails: null
    }, $scope.currentTabId = null, $scope.tabs = tabData.tabs, $scope.tabSubmitted = {}, $scope.startUpErrors = null, $scope.settings = settings, $scope.staticData = staticData, $scope.inputData = inputData, $scope.calculatedData = calculatedData, $scope.customerDetails = customerDetails, $scope.installationDetails = installationDetails, $scope.showDoorOverlappingModePicker = !0, $scope.price = price, $scope.previewSettings = previewSettings, $scope.interiorPicker = {
        suitableInteriors: [],
        numberOfInteriorsPerPage: 0,
        currentPage: 0,
        numberOfPages: 0,
        totalSuitableInteriors: 0,
        fromIndex: 0,
        toIndex: 0,
        pages: [],
        selectedInteriorIsUnsuitableForOpeningSize: !1
    }, $scope.cannotPlaceDoorPanelModal = {
        message: "",
        availableDoorPanelIds: [],
        doorIndex: 0,
        panelIndex: 0,
        doorSide: "Front"
    }, $scope.selectedDoorPanelId = null, $scope.selectedDoorPanelConfigurationId = null, $scope.linerQuantities = [0, 1, 2, 3], $scope.refreshCalculatedData = function() {
        calculatedData.doorQuantityRules = doorQuantityRulesCalculator.calculateDoorQuantityRules(), calculatedData.physicalMetrics = physicalMetricsCalculator.calculatePhysicalMetrics(), calculatedData.renderMetrics = renderMetricsCalculator.calculateRenderMetrics(), calculatedData.allowedDoorPanelIds = doorPanelHelper.gatherAllowedDoorPanelIds(), calculatedData.allowedDoorPanelIdsLookup = util.convertScalarArrayToHashTable(calculatedData.allowedDoorPanelIds), calculatedData.allowedDoorPanelIdsByMaterialType = staticData.buildDoorPanelIdsByMaterialTypeLookup(calculatedData.allowedDoorPanelIds), calculatedData.placementConstraintsForSelectedDoorPanelLookup = doorPanelPlacementConstraintsBuilder.buildPlacementConstraintsLookup($scope.selectedDoorPanelId), calculatedData.validity = validator.validateInputData()
    }, $scope.applyOpeningWidthChange = function() {
        inputData.openingSizeWasUserSelected = !0, $scope.handleOpeningSizeWidthChange()
    }, $scope.applyOpeningHeightChange = function() {
        inputData.openingSizeWasUserSelected = !0, $scope.handleOpeningSizeHeightChange()
    }, $scope.applyOverrideNumberOfSoftClosesChange = function() {
        inputData.trackSetOptions.numberOfSoftClosesOverride = inputData.trackSetOptions.overrideNumberOfSoftCloses ? 4 == inputData.doorOptions.numberOfDoors ? 6 : 4 : 0
    }, $scope.applyTrackSetWidthOverrideEnabledChange = function() {
        inputData.trackSetOptions.overrideTrackSetWidth || (inputData.trackSetOptions.trackSetWidthOverride = 0)
    }, $scope.chooseDoorRange = function(doorRangeId) {
        inputData.doorOptions.doorRangeId = doorRangeId, $scope.automaticallySelectDoorStileType(), $scope.automaticallySelectOpeningSize(), $scope.automaticallySelectNumberOfDoors(), $scope.automaticallySelectFrameMaterial(), $scope.automaticallySelectEndPanelMaterial(), $scope.automaticallySelectDoorOverlappingMode(), $scope.automaticallySelectDoorLiners(), $scope.automaticallySelectDoorPanel(), $scope.automaticallySelectTrackSets(), doorSelectionSanitiser.sanitiseDoorSelections()
    }, $scope.chooseEndPanelMaterial = function(materialId) {
        $scope.inputData.endPanelOptions.materialId = materialId
    }, $scope.chooseNumberOfDoors = function(numberOfDoors, wasUserSelected) {
        inputData.doorOptions.numberOfDoors = numberOfDoors, inputData.doorOptions.numberOfDoorsWasUserSelected = wasUserSelected, doorSelectionSanitiser.sanitiseDoorSelections(), $scope.automaticallySelectDoorLiners(), $scope.automaticallySelectTrackSets(), $scope.automaticallySelectDoorOverlappingMode()
    }, $scope.chooseDoorPanelConfiguration = function(chooseDoorPanelConfigurationId) {
        $scope.selectedDoorPanelConfigurationId = chooseDoorPanelConfigurationId
    }, $scope.chooseSoftCloseInclusion = function(includeSoftCloses) {
        inputData.trackSetOptions.includeSoftCloses = includeSoftCloses
    }, $scope.assignDoorPanelConfigurationToAllDoors = function() {
        for (var doorIndex = 0; doorIndex < inputData.doorOptions.numberOfDoors; doorIndex++) $scope.assignDoorPanelConfiguration(doorIndex)
    }, $scope.assignDoorPanelConfiguration = function(doorIndex) {
        var doorSelection = inputData.doorSelections[doorIndex];
        null == doorSelection && (doorSelection = {}, inputData.doorSelections[doorIndex] = doorSelection), doorSelection.doorPanelConfigurationId = $scope.selectedDoorPanelConfigurationId, doorSelection.doorPanelConfigurationWasUserSelected = !0, doorSelectionSanitiser.sanitiseDoorSelections()
    }, $scope.chooseDesiredDoorOverlappingMode = function(doorOverlappingMode) {
        $scope.inputData.doorOptions.desiredDoorOverlappingMode = doorOverlappingMode, $scope.automaticallySelectDoorOverlappingMode()
    }, $scope.chooseDoorPanel = function(doorPanelId) {
        $scope.selectedDoorPanelId = doorPanelId, $scope.refreshCalculatedData(), $scope.calculatedData.validity.hasErrors || previewRenderer.render()
    }, $scope.applyDoorPanel = function(doorIndex, panelIndex, doorSide, overrideDoorPanelId) {
        var selectedDoorPanel = staticData.doorPanelsById[overrideDoorPanelId || $scope.selectedDoorPanelId],
            doorPanelPlacementConstraints = doorPanelPlacementConstraintsBuilder.buildPlacementConstraintsForDoorPanelSelection(selectedDoorPanel.id, doorIndex, panelIndex, doorSide),
            doorPanelCanBeApplied = !0;
        if (doorPanelPlacementConstraints.desiredDoorPanelCanBeApplied) $("#configurator-cannot-place-door-panel-modal").modal("hide");
        else {
            var errorMessage = null;
            "DoorPanelNotAvailableForSelectedDoorRange" == doorPanelPlacementConstraints.reason ? errorMessage = scl.load("configurator-content/alerts/door-panel-not-available-for-selected-door-range") : "DoorPanelNotAllowedInThisLocation" == doorPanelPlacementConstraints.reason ? errorMessage = scl.load("configurator-content/alerts/door-panel-not-allowed-in-this-location") : "FrontSideIsDoubleSided" == doorPanelPlacementConstraints.reason ? errorMessage = scl.load("configurator-content/alerts/front-side-is-double-sided") : "OppositeSideIsSingleSidedThickAndDesiredPanelIsntSingleSided" == doorPanelPlacementConstraints.reason ? errorMessage = scl.load("configurator-content/alerts/opposite-side-is-single-sided-thick-and-desired-panel-isnt-single-sided") : "DoorPanelSelectionIsAutomated" == doorPanelPlacementConstraints.reason ? errorMessage = scl.load("configurator-content/alerts/door-panel-selection-is-automated") : "DoorsAreTooWideForDoorPanel" == doorPanelPlacementConstraints.reason ? errorMessage = scl.load("configurator-content/alerts/doors-are-too-wide-for-door-panel") : "DoorPanelConfigurationNotAllowedForDoorPanel" == doorPanelPlacementConstraints.reason ? errorMessage = scl.load("configurator-content/alerts/door-panel-configuration-not-allowed-for-door-panel") : "DoorPanelMustMatchFrontSideDoorPanelOrBeExceptionDoorPanel" == doorPanelPlacementConstraints.reason && (errorMessage = scl.load("configurator-content/alerts/door-panel-must-match-front-side-door-panel-or-be-exception-door-panel")), null != errorMessage && ($scope.cannotPlaceDoorPanelModal.message = $sce.trustAsHtml(errorMessage), $scope.cannotPlaceDoorPanelModal.availableDoorPanelIds = doorPanelPlacementConstraints.availableDoorPanelIds, $scope.cannotPlaceDoorPanelModal.doorIndex = doorIndex, $scope.cannotPlaceDoorPanelModal.panelIndex = panelIndex, $scope.cannotPlaceDoorPanelModal.doorSide = doorSide, $("#configurator-cannot-place-door-panel-modal").modal("show")), doorPanelCanBeApplied = !1
        }
        if (doorPanelCanBeApplied) {
            var selectedDoorPanelSidedess = doorPanelHelper.getDoorPanelEffectiveSidedness(selectedDoorPanel);
            "DoubleSided" == selectedDoorPanelSidedess && (doorSide = "Front");
            var panelSelection = $scope.inputData.doorSelections[doorIndex].panelSelections[panelIndex];
            panelSelection.doorPanelIdsByDoorSide[doorSide] = selectedDoorPanel.id, inputData.doorOptions.atLeastOneDoorPanelWasUserSelected = !0, "Front" == doorSide ? panelSelection.frontSideWasUserSelected = !0 : "Back" == doorSide && (panelSelection.backSideWasUserSelected = !0), "RoomDivider" == $scope.inputData.mode && "Front" == doorSide && "SingleSided" == selectedDoorPanelSidedess && (panelSelection.backSideWasUserSelected || (panelSelection.doorPanelIdsByDoorSide.Back = selectedDoorPanel.id)), doorSelectionSanitiser.sanitiseDoorSelections()
        }
    }, $scope.toggleLeftEndPanel = function() {
        $scope.inputData.endPanelOptions.hasLeftEndPanel = !$scope.inputData.endPanelOptions.hasLeftEndPanel, $scope.automaticallySelectDoorLiners()
    }, $scope.toggleRightEndPanel = function() {
        $scope.inputData.endPanelOptions.hasRightEndPanel = !$scope.inputData.endPanelOptions.hasRightEndPanel, $scope.automaticallySelectDoorLiners()
    }, $scope.chooseTrackSetTypeSelectionMode = function(trackSetTypeSelectionMode) {
        inputData.trackSetOptions.trackSetTypeSelectionMode = trackSetTypeSelectionMode, $scope.automaticallySelectTrackSets()
    }, $scope.chooseTrackSetType = function(trackSetType) {
        inputData.trackSetOptions.desiredTrackSetType = trackSetType, inputData.trackSetOptions.trackSetType = trackSetType
    }, $scope.chooseTrackSetMountingType = function(trackSetMountingType) {
        inputData.trackSetOptions.desiredTrackSetMountingType = trackSetMountingType, inputData.trackSetOptions.trackSetMountingType = trackSetMountingType
    }, $scope.chooseInteriorMaterial = function(materialId) {
        var interior = $scope.staticData.interiorsByProductId[$scope.inputData.interiorOptions.interiorProductId];
        $scope.inputData.interiorOptions.interiorProductVariantSku = interior.variantSkusByMaterialId[materialId], $scope.inputData.interiorOptions.interiorMaterialId = materialId
    }, $scope.chooseDoorLinerMaterial = function(materialId) {
        $scope.inputData.doorLinerOptions.materialId = materialId
    }, $scope.chooseInteriorProductRange = function(interiorProductRange) {
        $scope.inputData.interiorOptions.interiorProductRangeId = null != interiorProductRange ? interiorProductRange.id : null, $scope.inputData.interiorOptions.interiorProductId = null, $scope.inputData.interiorOptions.interiorProductVariantSku = null
    }, $scope.chooseInterior = function(interior) {
        if ($scope.inputData.interiorOptions.interiorProductId = interior.productId, null != $scope.inputData.interiorOptions.interiorMaterialId) {
            var variantSku = interior.variantSkusByMaterialId[$scope.inputData.interiorOptions.interiorMaterialId];
            $scope.inputData.interiorOptions.interiorProductVariantSku = null != variantSku ? variantSku : null
        }
    }, $scope.chooseEndPanelMaterialSelectionMode = function(materialSelectionMode) {
        $scope.inputData.endPanelOptions.materialSelectionMode = materialSelectionMode, $scope.automaticallySelectEndPanelMaterial()
    }, $scope.chooseDoorLinerSelectionMode = function(doorLinerSelectionMode) {
        $scope.inputData.doorLinerOptions.selectionMode = doorLinerSelectionMode, $scope.automaticallySelectDoorLiners()
    }, $scope.chooseFrameMaterial = function(materialId, wasUserSelected) {
        $scope.inputData.doorOptions.doorFrameMaterialId = materialId, $scope.inputData.doorOptions.doorFrameMaterialWasUserSelected = wasUserSelected, $scope.automaticallySelectEndPanelMaterial(), $scope.automaticallySelectDoorLiners(), $scope.automaticallySelectTrackSets()
    }, $scope.chooseDesiredStileType = function(stileType) {
        $scope.inputData.doorOptions.desiredDoorStileTypeId = stileType.doorStileTypeId, $scope.automaticallySelectDoorStileType()
    }, $scope.chooseInstallationOptInChoice = function(installationOptInChoice) {
        installationDetails.optInChoice = installationOptInChoice, "No" == installationDetails.optInChoice && (installationDetails.hasAcceptedTermsAndConditions = !1), "Wardrobe" == staticData.mode && $scope.settings.linersAreRequiredForInstallation && "NoLiners" == inputData.doorLinerOptions.selectionMode && (inputData.doorLinerOptions.selectionMode = "Automatic", $scope.automaticallySelectDoorLiners())
    }, $scope.toggleDoorPanelConfigurationValidation = function(enable) {
        inputData.validationSettings.validateDoorPanelConfigurations = enable, $scope.handleValidationSettingChange()
    }, $scope.toggleFrameMaterialValidation = function(enable) {
        inputData.validationSettings.validateFrameMaterial = enable, $scope.handleValidationSettingChange()
    }, $scope.toggleDoorPanelPlacementValidation = function(enable) {
        inputData.validationSettings.validateDoorPanelPlacements = enable, $scope.handleValidationSettingChange()
    }, $scope.automaticallySelectOpeningSize = function() {
        if (!inputData.openingSizeWasUserSelected) {
            var openingSizeRules = openingSizeRulesHelper.getOpeningSizeRules();
            inputData.openingSize.width < openingSizeRules.minimumOpeningSize.width ? inputData.openingSize.width = openingSizeRules.minimumOpeningSize.width : inputData.openingSize.width > openingSizeRules.maximumOpeningSize.width && (inputData.openingSize.width = openingSizeRules.maximumOpeningSize.width), inputData.openingSize.height < openingSizeRules.minimumOpeningSize.height ? inputData.openingSize.height = openingSizeRules.minimumOpeningSize.height : inputData.openingSize.height > openingSizeRules.maximumOpeningSize.height && (inputData.openingSize.height = openingSizeRules.maximumOpeningSize.height)
        }
    }, $scope.automaticallySelectFrameMaterial = function() {
        if (!$scope.inputData.doorOptions.doorFrameMaterialWasUserSelected) {
            var doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId]; - 1 == util.inArray(doorRange.allowedFrameMaterialIds, inputData.doorOptions.doorFrameMaterialId) && (inputData.doorOptions.doorFrameMaterialId = doorRange.allowedFrameMaterialIds[0])
        }
    }, $scope.automaticallySelectDoorLiners = function(forceAutomaticSelectionMode) {
        var requiredDoorLinerQuantities = doorLinerQuantityCalculator.calculateRequiredDoorLinerQuantities(forceAutomaticSelectionMode ? "Automatic" : inputData.doorLinerOptions.selectionMode, inputData.doorOptions.numberOfDoors);
        for (var placementLocation in requiredDoorLinerQuantities) inputData.doorLinerOptions.quantitiesByPlacementLocation[placementLocation] = requiredDoorLinerQuantities[placementLocation];
        if ("Automatic" == inputData.doorLinerOptions.selectionMode || forceAutomaticSelectionMode) {
            var materialId = materialHelper.coalesceMaterial(inputData.doorOptions.doorFrameMaterialId, staticData.doorRangesById[inputData.doorOptions.doorRangeId].allowedLinerMaterialIds);
            null != materialId && (inputData.doorLinerOptions.materialId = materialId)
        }
        inputData.doorLinerOptions.horizontalQuantityMultiplier = Math.ceil(inputData.openingSize.width / staticData.maxLinerLength), inputData.doorLinerOptions.verticalQuantityMultiplier = Math.ceil(inputData.openingSize.height / staticData.maxLinerLength)
    }, $scope.automaticallySelectTrackSets = function() {
        var doorRange = $scope.staticData.doorRangesById[$scope.inputData.doorOptions.doorRangeId];
        "Automatic" == inputData.trackSetOptions.trackSetTypeSelectionMode && (1 == inputData.doorOptions.numberOfDoors ? (inputData.trackSetOptions.desiredTrackSetType = "Single", inputData.trackSetOptions.trackSetType = "Single") : (inputData.trackSetOptions.desiredTrackSetType = "Double", inputData.trackSetOptions.trackSetType = "Double")), inputData.trackSetOptions.trackSetType = null == doorRange.allowedTrackSetTypesLookup[inputData.trackSetOptions.desiredTrackSetType] ? null != doorRange.allowedTrackSetTypesLookup.Double ? "Double" : doorRange.allowedTrackSetTypes[0] : inputData.trackSetOptions.desiredTrackSetType, inputData.trackSetOptions.trackSetMountingType = 0 == doorRange.allowedTrackSetMountingTypes.length ? "Unknown" : null == doorRange.allowedTrackSetMountingTypesLookup[inputData.trackSetOptions.desiredTrackSetMountingType] ? doorRange.allowedTrackSetMountingTypes[0] : inputData.trackSetOptions.desiredTrackSetMountingType, inputData.trackSetOptions.overrideTrackSetWidth || (inputData.trackSetOptions.trackSetWidthOverride = inputData.openingSize.width), null == doorRange.allowedTrackSetTypesLookup.Single && (inputData.trackSetOptions.numberOfAdditionalSingleTrackSets = 0), null == doorRange.allowedTrackSetTypesLookup.Double && (inputData.trackSetOptions.numberOfAdditionalDoubleTrackSets = 0), null == doorRange.allowedTrackSetTypesLookup.Triple && (inputData.trackSetOptions.numberOfAdditionalTripleTrackSets = 0)
    }, $scope.automaticallySelectEndPanelMaterial = function() {
        if ("Manual" != inputData.endPanelOptions.materialSelectionMode) {
            var doorRange = $scope.staticData.doorRangesById[$scope.inputData.doorOptions.doorRangeId],
                materialId = materialHelper.coalesceMaterial($scope.inputData.doorOptions.doorFrameMaterialId, doorRange.allowedEndPanelMaterialIds);
            null != materialId && ($scope.inputData.endPanelOptions.materialId = materialId)
        }
    }, $scope.automaticallySelectDoorOverlappingMode = function() {
        var doorQuantityDetails = doorQuantityRulesCalculator.calculateDoorQuantityDetails(inputData.doorOptions.numberOfDoors);
        if (doorQuantityDetails.allowedOverlappingModes.length > 0) {
            var desiredDoorOverlappingModeIsSuitable = -1 != util.inArray(doorQuantityDetails.allowedOverlappingModes, inputData.doorOptions.desiredDoorOverlappingMode);
            inputData.doorOptions.doorOverlappingMode = desiredDoorOverlappingModeIsSuitable ? inputData.doorOptions.desiredDoorOverlappingMode : doorQuantityDetails.allowedOverlappingModes[0]
        } else inputData.doorOptions.desiredDoorOverlappingMode = "Interleaving"
    }, $scope.automaticallySelectDoorStileType = function() {
        var doorRange = $scope.staticData.doorRangesById[$scope.inputData.doorOptions.doorRangeId];
        if (0 == doorRange.stileTypes.length) inputData.doorOptions.doorStileTypeId = null;
        else {
            for (var desiredDoorStileTypeIsAcceptable = !1, i = 0; i < doorRange.stileTypes.length; i++) {
                var stileType = doorRange.stileTypes[i];
                if (null != stileType.doorStileTypeId && stileType.doorStileTypeId == inputData.doorOptions.desiredDoorStileTypeId) {
                    desiredDoorStileTypeIsAcceptable = !0;
                    break
                }
            }
            inputData.doorOptions.doorStileTypeId = desiredDoorStileTypeIsAcceptable ? inputData.doorOptions.desiredDoorStileTypeId : doorRange.stileTypes[0].doorStileTypeId
        }
    }, $scope.automaticallySelectNumberOfDoors = function() {
        if (!$scope.inputData.doorOptions.numberOfDoorsWasUserSelected) {
            var physicalMetrics = physicalMetricsCalculator.calculatePhysicalMetrics(),
                doorQuantityRules = doorQuantityRulesCalculator.calculateDoorQuantityRules(physicalMetrics);
            $scope.inputData.doorOptions.numberOfDoors < doorQuantityRules.minNumberOfDoors ? $scope.inputData.doorOptions.numberOfDoors = doorQuantityRules.minNumberOfDoors : $scope.inputData.doorOptions.numberOfDoors > doorQuantityRules.maxNumberOfDoors && ($scope.inputData.doorOptions.numberOfDoors = doorQuantityRules.maxNumberOfDoors)
        }
    }, $scope.automaticallySelectDoorPanel = function() {
        var allowedDoorPanelIds = ($scope.staticData.doorRangesById[$scope.inputData.doorOptions.doorRangeId], doorPanelHelper.gatherAllowedDoorPanelIds());
        allowedDoorPanelIds.length > 0 ? -1 == util.inArray(allowedDoorPanelIds, $scope.selectedDoorPanelId) && ($scope.selectedDoorPanelId = allowedDoorPanelIds[0]) : $scope.selectedDoorPanelId = $scope.staticData.doorPanels[0].id
    }, $scope.autoFixOpeningWidth = function() {
        var fixedUpWidth = null,
            openingSizeRules = openingSizeRulesHelper.getOpeningSizeRules();
        inputData.openingSize.width < openingSizeRules.minimumOpeningSize.width ? fixedUpWidth = openingSizeRules.minimumOpeningSize.width : inputData.openingSize.width > openingSizeRules.maximumOpeningSize.width && (fixedUpWidth = openingSizeRules.maximumOpeningSize.width), null != fixedUpWidth && (inputData.openingSize.width = fixedUpWidth, $scope.handleOpeningSizeWidthChange())
    }, $scope.autoFixOpeningHeight = function() {
        var fixedUpHeight = null,
            openingSizeRules = openingSizeRulesHelper.getOpeningSizeRules();
        inputData.openingSize.height < openingSizeRules.minimumOpeningSize.height ? fixedUpHeight = openingSizeRules.minimumOpeningSize.height : inputData.openingSize.height > openingSizeRules.maximumOpeningSize.height && (fixedUpHeight = openingSizeRules.maximumOpeningSize.height), null != fixedUpHeight && (inputData.openingSize.height = fixedUpHeight, $scope.handleOpeningSizeHeightChange())
    }, $scope.autoFixNumberOfDoors = function() {
        var doorQuantityRules = calculatedData.doorQuantityRules;
        inputData.doorOptions.numberOfDoors < doorQuantityRules.minNumberOfDoors ? $scope.chooseNumberOfDoors(doorQuantityRules.minNumberOfDoors, !1) : inputData.doorOptions.numberOfDoors > doorQuantityRules.maxNumberOfDoors && $scope.chooseNumberOfDoors(doorQuantityRules.maxNumberOfDoors, !1)
    }, $scope.autoFixFrameMaterialSelection = function() {
        var doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId]; - 1 == util.inArray(doorRange.allowedFrameMaterialIds, inputData.doorOptions.doorFrameMaterialId) && $scope.chooseFrameMaterial(doorRange.allowedFrameMaterialIds[0], !1)
    }, $scope.autoFixDoorPanelConfigurationSelections = function() {
        for (var doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], doorIndex = 0; doorIndex < inputData.doorSelections.length; doorIndex++) {
            var doorSelection = inputData.doorSelections[doorIndex]; - 1 == util.inArray(doorRange.allowedDoorPanelConfigurationIds, doorSelection.doorPanelConfigurationId) && (doorSelection.doorPanelConfigurationId = doorRange.allowedDoorPanelConfigurationIds[0])
        }
        doorSelectionSanitiser.sanitiseDoorSelections()
    }, $scope.autoFixLinersRequiredForInstallation = function() {
        $scope.settings.disableAutomaticLinerSelection || (inputData.doorLinerOptions.selectionMode = "Automatic"), $scope.automaticallySelectDoorLiners(!0)
    }, $scope.moveToNextInteriorPickerPage = function() {
        $scope.interiorPicker.currentPage++, $scope.calculateInteriorPickerIndexes()
    }, $scope.moveToPreviousInteriorPickerPage = function() {
        $scope.interiorPicker.currentPage--, $scope.calculateInteriorPickerIndexes()
    }, $scope.gotoInteriorPickerPage = function(page) {
        $scope.interiorPicker.currentPage = page, $scope.calculateInteriorPickerIndexes()
    }, $scope.calculateInteriorPickerIndexes = function() {
        $scope.interiorPicker.fromIndex = ($scope.interiorPicker.currentPage - 1) * $scope.interiorPicker.numberOfInteriorsPerPage, $scope.interiorPicker.toIndex = $scope.interiorPicker.fromIndex + $scope.interiorPicker.numberOfInteriorsPerPage - 1
    }, $scope.refreshInteriorPicker = function() {
        $scope.interiorPicker.availableInteriors = [];
        for (var i = 0; i < staticData.interiors.length; i++) {
            var interior = staticData.interiors[i];
            $scope.checkIfInteriorShouldBeIncludedInPickerList(interior) && $scope.interiorPicker.availableInteriors.push(interior)
        }
        $scope.interiorPicker.numberOfInteriorsPerPage = 4, $scope.interiorPicker.currentPage = 1, $scope.interiorPicker.numberOfPages = Math.ceil($scope.interiorPicker.availableInteriors.length / $scope.interiorPicker.numberOfInteriorsPerPage), $scope.interiorPicker.pages = [];
        for (var i = 0; i < $scope.interiorPicker.numberOfPages; i++) $scope.interiorPicker.pages.push(i + 1);
        $scope.calculateInteriorPickerIndexes(), $scope.interiorPicker.selectedInteriorIsUnsuitableForOpeningSize = $scope.checkSelectedInteriorIsUnsuitableForOpeningSize()
    }, $scope.checkIfInteriorShouldBeIncludedInPickerList = function(interior) {
        var include = !0;
        interior.productRangeId != $scope.inputData.interiorOptions.interiorProductRangeId && (include = !1);
        var interiorIsSuitableForOpeningSize = $scope.checkInteriorIsSuitableForOpeningSize(interior),
            interiorIsSelected = null != inputData.interiorOptions.interiorMaterialId && interior.productId == inputData.interiorOptions.interiorProductId;
        return interiorIsSuitableForOpeningSize || interiorIsSelected || (include = !1), include
    }, $scope.checkInteriorIsSuitableForOpeningSize = function(interior) {
        var interiorIsSuitableForOpeningSize = (0 == interior.minimumOpeningWidth || $scope.inputData.openingSize.width >= interior.minimumOpeningWidth) && (0 == interior.maximumOpeningWidth || $scope.inputData.openingSize.width <= interior.maximumOpeningWidth);
        return interiorIsSuitableForOpeningSize
    }, $scope.checkSelectedInteriorIsUnsuitableForOpeningSize = function() {
        var isUnsuitable = !1;
        if (null != inputData.interiorOptions.interiorMaterialId && null != inputData.interiorOptions.interiorProductId) {
            var selectedInterior = staticData.interiorsByProductId[inputData.interiorOptions.interiorProductId];
            isUnsuitable = !$scope.checkInteriorIsSuitableForOpeningSize(selectedInterior)
        }
        return isUnsuitable
    }, $scope.doorQuantityIsAvailable = function(doorQuantity) {
        var doorQuantityRules = $scope.calculatedData.doorQuantityRules;
        return doorQuantity >= doorQuantityRules.minNumberOfDoors && doorQuantity <= doorQuantityRules.maxNumberOfDoors
    }, $scope.linerSelectionModeIsSuitable = function(linerSelectionMode) {
        var modeIsSuitable = !0;
        return "NoLiners" == linerSelectionMode.mode && "Yes" == installationDetails.optInChoice && (modeIsSuitable = !1), linerSelectionMode.requiresPermission && !staticData.permissions.canOverrideLinerQuantitiesAndMaterial && (modeIsSuitable = !1), modeIsSuitable
    }, $scope.getDoorPanelConfigurationSortIndexById = function(doorPanelConfigurationId) {
        var doorPanelConfiguration = $scope.staticData.doorPanelConfigurationsById[doorPanelConfigurationId];
        return null != doorPanelConfiguration ? doorPanelConfiguration.details.sortIndex : 0
    }, $scope.submitToBasket = function() {
        submitter.submitToBasket(function(result) {
            "Ok" == result.status ? $window.location.href = staticData.basketUrl : "BasketReadOnly" == result.status && uiHelper.alert(staticData.wardrobeConfigurationAlreadyExistsInBasket ? {
                heading: "Unable to update design",
                message: "Sorry, the design couldn't be updated. Your basket is currently read-only."
            } : {
                heading: "Unable to add design",
                message: "Sorry, the design couldn't be added. Your basket is currently read-only."
            })
        })
    }, $scope.previewCutSheet = function() {
        submitter.generateCutSheet()
    }, $scope.previewRouteLines = function() {
        submitter.generateRouteLines()
    }, $scope.print = function() {
        submitter.generatePrintableSummary()
    }, $scope.saveForCustomer = function() {
        submitter.saveForCustomer();
        var placeholderReplacements = {
                "customer-email-address": $scope.customerDetails.emailAddress
            },
            baseContentId = "customer-save-wardrobe-configuration" + (util.stringIsNullOrWhiteSpace($scope.customerDetails.emailAddress) ? "-without-email-address" : "-with-email-address");
        $rootScope.$broadcast("alert", {
            heading: scl.load("configurator-content/alerts/" + baseContentId + "-heading", placeholderReplacements),
            message: scl.load("configurator-content/alerts/" + baseContentId + "-message", placeholderReplacements)
        })
    }, $scope.saveForSalesUser = function(sendEmailToCustomer) {
        var epilogue = function() {
            submitter.saveForSalesUser(sendEmailToCustomer);
            var placeholderReplacements = {
                    "customer-email-address": $scope.customerDetails.emailAddress
                },
                baseContentId = "sales-user-save-wardrobe-configuration" + (sendEmailToCustomer ? "-and-send-to-customer" : "");
            $rootScope.$broadcast("alert", {
                heading: scl.load("configurator-content/alerts/" + baseContentId + "-heading", placeholderReplacements),
                message: scl.load("configurator-content/alerts/" + baseContentId + "-message", placeholderReplacements)
            })
        };
        if (sendEmailToCustomer) {
            var placeholderReplacements = {
                    "customer-email-address": $scope.customerDetails.emailAddress
                },
                baseContentId = "sales-user-confirm-save-wardrobe-configuration" + (sendEmailToCustomer ? "-and-send-to-customer" : "");
            uiHelper.alert({
                mode: "yes-no",
                heading: scl.load("configurator-content/alerts/" + baseContentId + "-heading", placeholderReplacements),
                message: scl.load("configurator-content/alerts/" + baseContentId + "-message", placeholderReplacements),
                confirmedCallback: function() {
                    epilogue()
                }
            })
        } else epilogue()
    }, $scope.handleOpeningSizeWidthChange = function() {
        $scope.automaticallySelectNumberOfDoors(), doorSelectionSanitiser.sanitiseDoorSelections(), $scope.automaticallySelectTrackSets(), $scope.automaticallySelectDoorOverlappingMode(), $scope.automaticallySelectDoorLiners()
    }, $scope.handleOpeningSizeHeightChange = function() {
        $scope.automaticallySelectDoorLiners()
    }, $scope.handleValidationSettingChange = function() {
        $scope.refreshCalculatedData(), $scope.calculatedData.validity.hasErrors || previewRenderer.render()
    }, $scope.reset = function() {
        $rootScope.$broadcast("alert", {
            heading: scl.load("configurator-content/alerts/reset-configuration-heading"),
            message: scl.load("configurator-content/alerts/reset-configuration-message"),
            mode: "yes-no",
            confirmedCallback: function() {
                $scope.resetInputData(), $scope.showTab("dimensions"), previewSettings.enableEditMode = !1
            }
        })
    }, $scope.manageAttachedDocuments = function() {
        $("#configurator-attached-documents-modal").modal("show")
    }, $scope.addAttachedDocument = function() {
        $("#configurator-attached-documents-modal").modal("hide"), $rootScope.$broadcast("choose-attached-document", {
            callback: function(attachedDocument) {
                null != attachedDocument && -1 == util.inArray(inputData.attachedDocumentIds, attachedDocument.id) && (staticData.attachedDocumentsById[attachedDocument.id] = attachedDocument, inputData.attachedDocumentIds.push(attachedDocument.id)), $("#configurator-attached-documents-modal").modal("show")
            }
        })
    }, $scope.removeAttachedDocument = function(index) {
        inputData.attachedDocumentIds.splice(index, 1)
    }, $scope.downloadAttachedDocument = function(attachedDocumentId) {
        $window.location = dataUrlBuilder.buildDataUrl("/attached-document-api/DownloadAttachedDocument", {
            attachedDocumentId: attachedDocumentId
        })
    }, $scope.dumpInputDataJson = function() {
        console.log(JSON.stringify($scope.inputData))
    }, $scope.handlePreviewContainerClick = function(e) {
        var previewContainerPosition = domManager.getElementPositionRelativeToViewport("preview-container");
        previewHotSpotManager.handlePreviewContainerClick(e.clientX - previewContainerPosition.x, e.clientY - previewContainerPosition.y)
    }, $scope.resizeCanvas = function() {
        var container = $("#preview-container");
        console.log("Resize canvas: " + container.width() + "x" + container.height());
        var c = $("#preview-canvas");
        c.attr("width", container.width()), c.attr("height", container.height()), previewRenderer.render()
    }, $scope.showTab = function(tabId, options) {
        if (options = options || {}, null != $scope.currentTabId) {
            if ($scope.tabSubmitted[$scope.currentTabId] = !0, $scope.setPresentFormsAsSubmitted(), !$scope.determineIfUserCanProgressThroughTabs()) return;

            var currentTabValidator = $scope.tabValidators[$scope.currentTabId];
            if (null != currentTabValidator && !currentTabValidator()) return;
            "dimensions" != $scope.currentTabId || util.stringIsNullOrWhiteSpace(customerDetails.emailAddress) || submitter.upsertWardrobeConfiguratorCustomer()
        }
        $scope.currentTabId = tabId, previewSettings.enableEditMode = "door-panels" == tabId, util.scrollToElement("#configurator-scroll-to-point", -5), options.dontPushHistoryState || history.pushState({
            tabId: tabId
        }, tabId), null != options.lookAtDoorSide && (previewSettings.lookAtDoorSide = options.lookAtDoorSide), util.sendGaVirtualPageView("configurator-" + tabId)
    }, $scope.gotoPreviousTab = function() {
        var iteratorTab = tabData.tabsById[$scope.currentTabId];
        do iteratorTab = iteratorTab.previousTab; while (!iteratorTab.enabled());
        $scope.showTab(iteratorTab.id)
    }, $scope.gotoNextTab = function() {
        var iteratorTab = tabData.tabsById[$scope.currentTabId];
        do iteratorTab = iteratorTab.nextTab; while (!iteratorTab.enabled());
        $scope.showTab(iteratorTab.id)
    }, $scope.setPresentFormsAsSubmitted = function() {
        for (formName in $scope.forms) {
            var form = $scope.forms[formName];
            null != form && form.$setSubmitted()
        }
    }, $scope.determineIfAnyPresentFormsAreInvalid = function() {
        for (formName in $scope.forms) {
            var form = $scope.forms[formName];
            if (null != form && form.$invalid) return !0
        }
        return !1
    }, $scope.determineIfUserCanProgressThroughTabs = function() {
        return !$scope.determineIfAnyPresentFormsAreInvalid()
    }, $scope.getTooltip = function(tooltipId) {
        return $("#\\/html\\/configurator-content\\/" + tooltipId + "-tooltip\\.html").html()
    }, $scope.buildMaterialTooltip = function(materialId) {
        var tooltip = (staticData.materialsById[materialId], staticData.materialDisplayNamesById[materialId]);
        return tooltip
    }, $scope.translate = function(text, args) {
        return translator.translate(text, args)
    }, $scope.tabValidators = {}, $scope.tabValidators.interior = function() {
        var isValid = !0;
        return null != inputData.interiorOptions.interiorProductId && null == inputData.interiorOptions.interiorMaterialId && (isValid = !1), isValid
    }, $scope.tabValidators.installation = function() {
        var isValid = !0;
        return "Yes" != installationDetails.optInChoice || installationDetails.hasAcceptedTermsAndConditions || (isValid = !1), isValid
    }, $scope.init = function(mode) {
        "load-from-query-string" == mode && (mode = util.getQueryStringParameter("mode")), previewSettings.doorPanelClickHandler = function(doorIndex, panelIndex, doorSide) {
            $scope.applyDoorPanel(doorIndex, panelIndex, doorSide)
        }, null != mode && "" != mode.trim() ? $scope.loadPhase1(mode) : $scope.subView = "launch"
    }, $scope.launch = function(mode) {
        $scope.loadPhase1(mode)
    }, $scope.loadStaticData = function(mode, onLoaded) {
        $scope.staticData.load({
            mode: mode,
            onLoaded: function() {
                null != onLoaded && onLoaded()
            },
            onNoDoorRangesAvailable: function() {
                $scope.subView = "no-door-ranges-available"
            },
            onFailed: function(startUpErrors) {
                $scope.subView = "error", $scope.startUpErrors = startUpErrors
            }
        })
    }, $scope.loadPhase1 = function(mode) {
        $scope.subView = "loading", $scope.loadStaticData(mode, function() {
            util.copyObjectPropertiesToOtherObject(staticData.configuratorSettings, $scope.settings), util.copyObjectPropertiesToOtherObject($scope.staticData.customerDetails, $scope.customerDetails), submissionDetails.sourceSavedWardrobeConfigurationId = staticData.sourceSavedWardrobeConfigurationId;
            var initialInstallationDetails = util.deepCopy(staticData.initialInstallationDetails);
            util.copyObjectPropertiesToOtherObject(initialInstallationDetails, $scope.installationDetails), $scope.resetInputData(), previewDoorsRenderer.init(), previewHotSpotManager.init(), previewRenderer.init(), previewRasteriser.init(), renderingImageManager.init(function() {
                $scope.loadPhase2()
            })
        })
    }, $scope.loadPhase2 = function() {
        $scope.subView = "main";
        var stillFinalisingInit = !0,
            inputDataChangeHandler = function() {
                return stillFinalisingInit ? void console.log("Burned input data") : (console.log("Input data changed"), $scope.refreshCalculatedData(), $scope.refreshInteriorPicker(), void($scope.calculatedData.validity.hasErrors || (priceCalculator.recalculatePrice(), previewRenderer.render())))
            };
        $scope.$watch("inputData", inputDataChangeHandler, !0), $scope.$watch("installationDetails", inputDataChangeHandler, !0), $scope.$watch("previewSettings", function() {
            return stillFinalisingInit ? void console.log("Burned preview settings") : (console.log("Preview settings changed"), void($scope.calculatedData.validity.hasErrors || previewRenderer.render()))
        }, !0), $(window).resize(function() {
            $scope.resizeCanvas()
        }), window.onpopstate = function(arg) {
            var tabId = null != arg.state ? arg.state.tabId : "dimensions";
            console.log(tabId), $scope.$apply(function() {
                $scope.showTab(tabId, {
                    dontPushHistoryState: !0
                })
            })
        }, $scope.$apply(), stillFinalisingInit = !1, priceCalculator.recalculatePrice($scope.inputData), window.requestAnimationFrame(function() {
            $scope.resizeCanvas()
        }), $scope.$on("price-viewing-options-changed", function() {
            priceCalculator.recalculatePrice(), $scope.loadStaticData($scope.staticData.mode, function() {
                $scope.refreshInteriorPicker()
            })
        }), $scope.showTab("dimensions", {
            dontPushHistoryState: !0
        })
    }, $scope.resetInputData = function() {
        var initialInputData = util.deepCopy(staticData.initialInputData);
        util.copyObjectPropertiesToOtherObject(initialInputData, $scope.inputData), $scope.automaticallySelectDoorStileType(), $scope.automaticallySelectOpeningSize(), $scope.automaticallySelectNumberOfDoors(), $scope.automaticallySelectFrameMaterial(), $scope.automaticallySelectDoorPanel(), $scope.automaticallySelectDoorLiners(), $scope.automaticallySelectTrackSets(), doorSelectionSanitiser.sanitiseDoorSelections(), $scope.automaticallySelectDoorOverlappingMode(), $scope.refreshInteriorPicker(), $scope.refreshCalculatedData()
    }
}]);
var RasterInstructionSymbols = {
    clear: 1,
    rect: 2,
    fillRect: 3,
    fillText: 4,
    drawClippedImage: 5,
    drawImage: 6
};
siteApp.factory("apertureSizeCalculator", ["staticData", "inputData", function(staticData, inputData) {
    return {
        calculateApertureSize: function(doorLinerQuantitiesByPlacementLocation) {
            null == doorLinerQuantitiesByPlacementLocation && (doorLinerQuantitiesByPlacementLocation = inputData.doorLinerOptions.quantitiesByPlacementLocation);
            var apertureSize = {
                width: inputData.openingSize.width,
                height: inputData.openingSize.height
            };
            return inputData.endPanelOptions.hasLeftEndPanel && (apertureSize.width -= staticData.endPanelThickness), inputData.endPanelOptions.hasRightEndPanel && (apertureSize.width -= staticData.endPanelThickness), apertureSize.width -= (doorLinerQuantitiesByPlacementLocation.Left + doorLinerQuantitiesByPlacementLocation.Right) * staticData.doorLinerThickness, apertureSize.height -= (doorLinerQuantitiesByPlacementLocation.Top + doorLinerQuantitiesByPlacementLocation.Bottom) * staticData.doorLinerThickness, apertureSize
        }
    }
}]), siteApp.factory("calculatedData", [function() {
    return console.log("Instantiated calculated data."), {
        physicalMetrics: null,
        renderMetrics: null,
        doorRules: null,
        placementConstraintsForSelectedDoorPanelLookup: null,
        validity: {
            inputDataIsInvalid: !1,
            openingTooThin: !1,
            openingTooWide: !1,
            openingTooShort: !1,
            openingTooTall: !1,
            tooFewDoors: !1,
            tooManyDoors: !1,
            doorFrameMaterialInvalid: !1,
            endPanelMaterialInvalid: !1
        }
    }
}]), siteApp.factory("conditionalDoorPanelSetConditionChecker", ["staticData", "inputData", "util", function(staticData, inputData, util) {
    return {
        checkConditionalDoorPanelSetConditions: function(conditionalDoorPanelSet, conditionParameters) {
            for (var allConditionsAreMet = !0, i = 0; i < conditionalDoorPanelSet.conditions.length; i++) {
                var condition = conditionalDoorPanelSet.conditions[i];
                if (!this.checkCondition(condition, conditionParameters)) {
                    allConditionsAreMet = !1;
                    break
                }
            }
            return allConditionsAreMet
        },
        checkIfDoorPanelAndDoorPanelConfigurationCombinationIsAllowedByConditionalDoorPanelSets: function(doorPanelId, doorPanelConfigurationId) {
            for (var combinationIsAllowed = !1, doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], i = 0; i < doorRange.conditionalDoorPanelSets.length && !combinationIsAllowed; i++) {
                var conditionalDoorPanelSet = doorRange.conditionalDoorPanelSets[i];
                if (-1 != util.inArray(conditionalDoorPanelSet.doorPanelIds, doorPanelId)) {
                    var conditionParameters = {
                        doorPanelConfigurationId: doorPanelConfigurationId
                    };
                    if (this.checkConditionalDoorPanelSetConditions(conditionalDoorPanelSet, conditionParameters)) {
                        combinationIsAllowed = !0;
                        break
                    }
                }
            }
            return combinationIsAllowed
        },
        checkCondition: function(condition, conditionParameters) {
            var conditionMet = !1;
            if ("RequiredDoorPanelConfiguration" == condition.conditionType)
                if (null == conditionParameters || null == conditionParameters.doorPanelConfigurationId)
                    for (var i = 0; i < inputData.doorSelections.length; i++) {
                        var doorSelection = inputData.doorSelections[i];
                        if (-1 != util.inArray(condition.doorPanelConfigurationIds, doorSelection.doorPanelConfigurationId)) {
                            conditionMet = !0;
                            break
                        }
                    } else conditionMet = -1 != util.inArray(condition.doorPanelConfigurationIds, conditionParameters.doorPanelConfigurationId);
                else "RequiredMinimumNumberOfDoors" == condition.conditionType && (conditionMet = inputData.doorOptions.numberOfDoors >= condition.minimumNumberOfDoors);
            return conditionMet
        }
    }
}]), siteApp.factory("conditionalDoorRangeConstructionCoefficientsResolver", ["staticData", "inputData", "util", function(staticData, inputData, util) {
    return {
        findApplicableDoorRangeConstructionCoefficients: function() {
            for (var doorRangeConstructionCoefficients = null, doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], applicableConditonalDoorRangeConstructionCoefficients = null, i = 0; i < doorRange.conditionalConstructionCoefficients.length; i++) {
                var conditionalDoorRangeConstructionCoefficients = doorRange.conditionalConstructionCoefficients[i];
                if (this.determineIfConditionalDoorRangeConstructionCoefficientsAreApplicable(conditionalDoorRangeConstructionCoefficients)) {
                    applicableConditonalDoorRangeConstructionCoefficients = conditionalDoorRangeConstructionCoefficients;
                    break
                }
            }
            return null != applicableConditonalDoorRangeConstructionCoefficients && (doorRangeConstructionCoefficients = applicableConditonalDoorRangeConstructionCoefficients.constructionCoefficients), null == doorRangeConstructionCoefficients && (console.error("Unable to resolve door range construction coefficients."), doorRangeConstructionCoefficients = {
                doorHeightAllowance: 0
            }), doorRangeConstructionCoefficients
        },
        determineIfConditionalDoorRangeConstructionCoefficientsAreApplicable: function(conditionalDoorRangeConstructionCoefficients) {
            for (var allConditonsPassed = !0, i = 0; i < conditionalDoorRangeConstructionCoefficients.conditions.length; i++) {
                var condition = conditionalDoorRangeConstructionCoefficients.conditions[i];
                if ("RequiredDoorStileType" == condition.conditionType && null != condition.doorStileTypeIds && (null == inputData.doorOptions.doorStileTypeId || -1 == util.inArray(condition.doorStileTypeIds, inputData.doorOptions.doorStileTypeId))) {
                    allConditonsPassed = !1;
                    break
                }
            }
            return allConditonsPassed
        }
    }
}]), siteApp.factory("customerDetails", [function() {
    return {}
}]), siteApp.factory("domManager", function() {
    return {
        addEventListenerToElement: function(elementId, eventType, callback) {
            var element = document.getElementById(elementId);
            element.addEventListener(eventType, callback, !1)
        },
        getElementPositionRelativeToViewport: function(elementId) {
            var element = document.getElementById(elementId),
                viewportOffset = element.getBoundingClientRect();
            return {
                x: viewportOffset.left,
                y: viewportOffset.top
            }
        },
        getElementSize: function(elementId) {
            var element = document.getElementById(elementId);
            return {
                width: element.clientWidth,
                height: element.clientHeight
            }
        }
    }
}), siteApp.factory("doorLinerQuantityCalculator", ["staticData", "inputData", "util", function(staticData, inputData) {
    return {
        calculateRequiredDoorLinerQuantities: function(selectionMode, numberOfDoors) {
            var linerQuantitiesByPlacementLocation = {
                Top: 0,
                Bottom: 0,
                Left: 0,
                Right: 0
            };
            if ("Wardrobe" == staticData.mode && staticData.configuratorSettings.enableLiners && numberOfDoors > 1)
                if ("Manual" == selectionMode)
                    for (var placementLocation in inputData.doorLinerOptions.quantitiesByPlacementLocation) linerQuantitiesByPlacementLocation[placementLocation] = inputData.doorLinerOptions.quantitiesByPlacementLocation[placementLocation];
                else("Automatic" == selectionMode || "AutomaticWithManualMaterial" == selectionMode) && (linerQuantitiesByPlacementLocation.Top = staticData.configuratorSettings.automaticallySelectedLinerQuantitiesByPlacementLocation.Top || 0, linerQuantitiesByPlacementLocation.Bottom = staticData.configuratorSettings.automaticallySelectedLinerQuantitiesByPlacementLocation.Bottom || 0, linerQuantitiesByPlacementLocation.Left = inputData.endPanelOptions.hasLeftEndPanel ? 0 : staticData.configuratorSettings.automaticallySelectedLinerQuantitiesByPlacementLocation.Left || 0, linerQuantitiesByPlacementLocation.Right = inputData.endPanelOptions.hasRightEndPanel ? 0 : staticData.configuratorSettings.automaticallySelectedLinerQuantitiesByPlacementLocation.Right || 0);
            return linerQuantitiesByPlacementLocation
        }
    }
}]), siteApp.factory("doorPanelConfigurationSelectionChecker", ["staticData", "inputData", "util", function(staticData, inputData, util) {
    return {
        identifyDoorsWithInvalidDoorPanelConfigurationSelection: function() {
            for (var invalidDoorIndexes = [], doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], doorIndex = 0; doorIndex < inputData.doorSelections.length; doorIndex++) {
                var doorSelection = inputData.doorSelections[doorIndex]; - 1 == util.inArray(doorRange.allowedDoorPanelConfigurationIds, doorSelection.doorPanelConfigurationId) && invalidDoorIndexes.push(doorIndex)
            }
            return invalidDoorIndexes
        }
    }
}]), siteApp.factory("doorPanelHelper", ["conditionalDoorPanelSetConditionChecker", "staticData", "inputData", "util", function(conditionalDoorPanelSetConditionChecker, staticData, inputData, util) {
    return {
        gatherAllowedDoorPanelIds: function() {
            for (var doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], applicableConditionalDoorPanelIds = [], i = 0; i < doorRange.conditionalDoorPanelSets.length; i++) {
                var conditionalDoorPanelSet = doorRange.conditionalDoorPanelSets[i];
                conditionalDoorPanelSetConditionChecker.checkConditionalDoorPanelSetConditions(conditionalDoorPanelSet) && util.addItemsToArray(applicableConditionalDoorPanelIds, conditionalDoorPanelSet.doorPanelIds)
            }
            var allowedDoorPanelIds = doorRange.allowedDoorPanelIds.concat(applicableConditionalDoorPanelIds);
            return allowedDoorPanelIds = util.dedupeArray(allowedDoorPanelIds)
        },
        getDoorPanelEffectiveSidedness: function(doorPanel) {
            var doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId];
            return "SingleSidedThick" == doorPanel.details.sidedness && doorRange.treatSingleSidedThickDoorPanelsAsDoubleSided ? "DoubleSided" : doorPanel.details.sidedness
        }
    }
}]), siteApp.factory("doorPanelPlacementAutomationRuleManager", ["staticData", "inputData", "util", function(staticData, inputData, util) {
    return {
        findApplicableDoorPanelPlacementAutomationRulesForPanelSelection: function(doorIndex, panelIndex) {
            for (var applicableAutomationRules = [], doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], doorSelection = inputData.doorSelections[doorIndex], i = 0; i < doorRange.doorPanelPlacementAutomationRules.length; i++) {
                var automationRule = doorRange.doorPanelPlacementAutomationRules[i]; - 1 != util.inArray(automationRule.applicableDoorPanelConfigurationIds, doorSelection.doorPanelConfigurationId) && -1 != util.inArray(automationRule.applicablePanelIndexes, panelIndex + 1) && applicableAutomationRules.push(automationRule)
            }
            return applicableAutomationRules
        },
        applyDoorPanelPlacementAutomationRulesToPanelSelection: function(automationRules, doorIndex, panelIndex) {
            for (var i = 0; i < automationRules.length; i++) {
                var automationRule = automationRules[i];
                this.applyDoorPanelPlacementAutomationRuleToPanelSelection(automationRule, doorIndex, panelIndex)
            }
        },
        applyDoorPanelPlacementAutomationRuleToPanelSelection: function(automationRule, doorIndex, panelIndex) {
            var targetPanelSelection = inputData.doorSelections[doorIndex].panelSelections[panelIndex];
            if ("MatchOtherPanel" == automationRule.automationType) {
                var sourcePanelSelection = inputData.doorSelections[doorIndex].panelSelections[automationRule.otherPanelIndex - 1];
                null != sourcePanelSelection && (targetPanelSelection.doorPanelIdsByDoorSide.Front = sourcePanelSelection.doorPanelIdsByDoorSide.Front, "RoomDivider" == inputData.mode && (targetPanelSelection.doorPanelIdsByDoorSide.Back = sourcePanelSelection.doorPanelIdsByDoorSide.Back))
            }
        }
    }
}]), siteApp.factory("doorPanelPlacementConstraintsBuilder", ["doorPanelPlacementAutomationRuleManager", "doorPanelHelper", "conditionalDoorPanelSetConditionChecker", "staticData", "inputData", "calculatedData", "util", function(doorPanelPlacementAutomationRuleManager, doorPanelHelper, conditionalDoorPanelSetConditionChecker, staticData, inputData, calculatedData, util) {
    return {
        buildPlacementConstraintsLookup: function(replacementDoorPanelId) {
            for (var constraintsLookup = {}, doorSides = "RoomDivider" == staticData.mode ? staticData.doorSides : ["Front"], doorIndex = 0; doorIndex < inputData.doorSelections.length; doorIndex++)
                for (var doorSelection = inputData.doorSelections[doorIndex], panelIndex = 0; panelIndex < doorSelection.panelSelections.length; panelIndex++)
                    for (var doorSideIndex = (doorSelection.panelSelections[panelIndex], 0); doorSideIndex < doorSides.length; doorSideIndex++) {
                        var doorSide = staticData.doorSides[doorSideIndex],
                            doorPanelPlacementConstraints = this.buildPlacementConstraintsForDoorPanelSelection(replacementDoorPanelId, doorIndex, panelIndex, doorSide);
                        constraintsLookup["doorIndex:" + doorIndex + "-panelIndex:" + panelIndex + "-doorSide:" + doorSide] = doorPanelPlacementConstraints
                    }
            return constraintsLookup
        },
        buildPlacementConstraintsForDoorPanelSelection: function(replacementDoorPanelId, doorIndex, panelIndex, doorSide) {
            var constraints = {
                    desiredDoorPanelCanBeApplied: !0,
                    doorPanelSelectionIsAutomated: !1,
                    reason: null,
                    doorSide: doorSide,
                    availableDoorPanelIds: []
                },
                doorSelection = inputData.doorSelections[doorIndex],
                panelSelection = doorSelection.panelSelections[panelIndex],
                currentDoorPanelId = panelSelection.doorPanelIdsByDoorSide[doorSide];
            if (constraints.doorPanelSelectionIsAutomated = this.checkIfDoorPanelSelectionIsAutomated(doorIndex, panelIndex), null == currentDoorPanelId && null == replacementDoorPanelId) {
                var doorPanelSelectionIsRequiredInThisLocation = this.checkIfDoorPanelSelectionIsRequiredAtLocation(doorIndex, panelIndex, doorSide);
                doorPanelSelectionIsRequiredInThisLocation && (constraints.desiredDoorPanelCanBeApplied = !1, constraints.reason = "DoorPanelSelectionIsRequiredInThisLocation")
            } else {
                var desiredDoorPanelId = replacementDoorPanelId || currentDoorPanelId;
                if (constraints.desiredDoorPanelCanBeApplied && null != desiredDoorPanelId) {
                    var desiredDoorPanelCanBeUsedWithSelectedDoorRange = this.checkIfDesiredDoorPanelCanBeUsedWithSelectedDoorRange(desiredDoorPanelId);
                    !desiredDoorPanelCanBeUsedWithSelectedDoorRange && inputData.validationSettings.validateDoorPanelPlacements && (constraints.desiredDoorPanelCanBeApplied = !1, constraints.reason = "DoorPanelNotAvailableForSelectedDoorRange")
                }
                if (constraints.desiredDoorPanelCanBeApplied && null != replacementDoorPanelId && constraints.doorPanelSelectionIsAutomated && (constraints.desiredDoorPanelCanBeApplied = !1, constraints.reason = "DoorPanelSelectionIsAutomated"), constraints.desiredDoorPanelCanBeApplied && "Back" == doorSide) {
                    var frontSideIsDoubleSided = this.checkIfDoorPanelFrontSideIsDoubleSided(doorIndex, panelIndex);
                    frontSideIsDoubleSided && (constraints.desiredDoorPanelCanBeApplied = !1, constraints.reason = "FrontSideIsDoubleSided")
                }
                if (constraints.desiredDoorPanelCanBeApplied && null != desiredDoorPanelId) {
                    var oppositeSide = "Front" == doorSide ? "Back" : "Front",
                        isMisMatch = this.checkIfDesiredDoorPanelIsNotSingleSidedAndOppositeSideIsSingleSidedThick(desiredDoorPanelId, doorIndex, panelIndex, oppositeSide);
                    isMisMatch && (constraints.desiredDoorPanelCanBeApplied = !1, constraints.reason = "OppositeSideIsSingleSidedThickAndDesiredPanelIsntSingleSided")
                }
                if (constraints.desiredDoorPanelCanBeApplied && null != desiredDoorPanelId) {
                    var rulesWereViolated = this.checkIfDesiredDoorPanelViolatesDoorPanelPlacementRules(desiredDoorPanelId, doorIndex, panelIndex);
                    rulesWereViolated && (constraints.desiredDoorPanelCanBeApplied = !1, constraints.reason = "DoorPanelNotAllowedInThisLocation")
                }
                if (constraints.desiredDoorPanelCanBeApplied && null != desiredDoorPanelId) {
                    var result = this.checkIfDesiredDoorPanelViolatesBackToBackDoorPanelPlacementRules(desiredDoorPanelId, doorIndex, panelIndex, doorSide);
                    result.ruleWasViolated && (constraints.desiredDoorPanelCanBeApplied = !1, constraints.reason = result.reason, constraints.availableDoorPanelIds = result.availableDoorPanelIds)
                }
                if (constraints.desiredDoorPanelCanBeApplied && null != desiredDoorPanelId) {
                    var doorsAreTooWide = this.checkIfDoorsAreTooWideForDesiredDoorPanel(desiredDoorPanelId);
                    doorsAreTooWide && inputData.validationSettings.validateDoorPanelPlacements && (constraints.desiredDoorPanelCanBeApplied = !1, constraints.reason = "DoorsAreTooWideForDoorPanel")
                }
                if (constraints.desiredDoorPanelCanBeApplied && null != desiredDoorPanelId) {
                    var doorPanelConfigurationIsAllowed = this.checkIfDoorPanelConfigurationIsAllowedForDesiredDoorPanel(desiredDoorPanelId, doorIndex);
                    !doorPanelConfigurationIsAllowed && inputData.validationSettings.validateDoorPanelPlacements && (constraints.desiredDoorPanelCanBeApplied = !1, constraints.reason = "DoorPanelConfigurationNotAllowedForDoorPanel")
                }
            }
            return constraints
        },
        checkIfDoorPanelSelectionIsRequiredAtLocation: function(doorIndex, panelIndex, doorSide) {
            var doorPanelSelectionIsRequiredInThisLocation = !1;
            if ("Front" == doorSide) doorPanelSelectionIsRequiredInThisLocation = !0;
            else {
                var panelSelection = inputData.doorSelections[doorIndex].panelSelections[panelIndex],
                    frontDoorPanelId = panelSelection.doorPanelIdsByDoorSide.Front,
                    frontDoorPanel = staticData.doorPanelsById[frontDoorPanelId],
                    frontDoorPanelSidedness = doorPanelHelper.getDoorPanelEffectiveSidedness(frontDoorPanel);
                "DoubleSided" != frontDoorPanelSidedness && (doorPanelSelectionIsRequiredInThisLocation = !0)
            }
            return doorPanelSelectionIsRequiredInThisLocation
        },
        checkIfDesiredDoorPanelCanBeUsedWithSelectedDoorRange: function(desiredDoorPanelId) {
            var desiredDoorPanel = staticData.doorPanelsById[desiredDoorPanelId],
                allowedDoorPanelIds = doorPanelHelper.gatherAllowedDoorPanelIds(),
                doorPanelIsAvailable = -1 != util.inArray(allowedDoorPanelIds, desiredDoorPanel.id);
            return doorPanelIsAvailable
        },
        checkIfDoorPanelSelectionIsAutomated: function(doorIndex, panelIndex) {
            var applicableAutomationRules = doorPanelPlacementAutomationRuleManager.findApplicableDoorPanelPlacementAutomationRulesForPanelSelection(doorIndex, panelIndex);
            return applicableAutomationRules.length > 0
        },
        checkIfDoorPanelFrontSideIsDoubleSided: function(doorIndex, panelIndex) {
            var panelSelection = inputData.doorSelections[doorIndex].panelSelections[panelIndex],
                frontDoorPanelId = panelSelection.doorPanelIdsByDoorSide.Front,
                frontDoorPanel = staticData.doorPanelsById[frontDoorPanelId],
                frontDoorPanelSidedness = doorPanelHelper.getDoorPanelEffectiveSidedness(frontDoorPanel),
                isDoubleSided = "DoubleSided" == frontDoorPanelSidedness;
            return isDoubleSided
        },
        checkIfDesiredDoorPanelIsNotSingleSidedAndOppositeSideIsSingleSidedThick: function(desiredDoorPanelId, doorIndex, panelIndex, oppositeSide) {
            var isMisMatch = !1,
                desiredDoorPanel = staticData.doorPanelsById[desiredDoorPanelId],
                desiredDoorPanelSidedness = doorPanelHelper.getDoorPanelEffectiveSidedness(desiredDoorPanel);
            if ("SingleSided" != desiredDoorPanelSidedness) {
                var panelSelection = inputData.doorSelections[doorIndex].panelSelections[panelIndex],
                    oppositeSideDoorPanelId = panelSelection.doorPanelIdsByDoorSide[oppositeSide];
                if (null != oppositeSideDoorPanelId) {
                    var oppositeSideDoorPanel = staticData.doorPanelsById[oppositeSideDoorPanelId],
                        oppositeSideDoorPanelSidedness = doorPanelHelper.getDoorPanelEffectiveSidedness(oppositeSideDoorPanel);
                    "SingleSidedThick" == oppositeSideDoorPanelSidedness && (isMisMatch = !0)
                }
            }
            return isMisMatch
        },
        checkIfDesiredDoorPanelViolatesDoorPanelPlacementRules: function(desiredDoorPanelId, doorIndex, panelIndex) {
            for (var desiredDoorPanel = staticData.doorPanelsById[desiredDoorPanelId], doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], i = 0; i < doorRange.doorPanelPlacementRules.length; i++) {
                var rule = doorRange.doorPanelPlacementRules[i];
                if (this.checkIfDoorPanelPlacementRuleIsViolated(rule, desiredDoorPanel, doorIndex, panelIndex)) return !0
            }
            return !1
        },
        checkIfDoorPanelPlacementRuleIsViolated: function(rule, desiredDoorPanel, doorIndex, panelIndex) {
            var doorPanelMaterial = staticData.materialsById[desiredDoorPanel.details.materialId],
                doorPanelConfiguration = staticData.doorPanelConfigurationsById[inputData.doorSelections[doorIndex].doorPanelConfigurationId];
            if (-1 == util.inArray(rule.applicableMaterialTypes, doorPanelMaterial.details.materialType)) return !1;
            for (var anyExceptionWasMatched = !1, i = 0; i < rule.exceptions.length; i++) {
                var exception = rule.exceptions[i];
                if (-1 != util.inArray(exception.applicableDoorPanelConfigurationIds, doorPanelConfiguration.id) && -1 != util.inArray(exception.applicablePanelIndexes, panelIndex + 1)) {
                    anyExceptionWasMatched = !0;
                    break
                }
            }
            var ruleWasVoilated = "AnyExcept" == rule.mode && anyExceptionWasMatched || "NoneExcept" == rule.mode && !anyExceptionWasMatched;
            return ruleWasVoilated
        },
        checkIfDesiredDoorPanelViolatesBackToBackDoorPanelPlacementRules: function(desiredDoorPanelId, doorIndex, panelIndex, doorSide) {
            for (var result = {
                    ruleWasViolated: !1
                }, desiredDoorPanel = staticData.doorPanelsById[desiredDoorPanelId], doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], i = 0; i < doorRange.backToBackDoorPanelPlacementRules.length; i++) {
                var rule = doorRange.backToBackDoorPanelPlacementRules[i];
                if (result = this.checkIfBackToBackDoorPanelPlacementRuleIsViolated(rule, desiredDoorPanel, doorIndex, panelIndex, doorSide), result.ruleWasViolated) break
            }
            return result
        },
        checkIfBackToBackDoorPanelPlacementRuleIsViolated: function(rule, desiredDoorPanel, doorIndex, panelIndex, doorSide) {
            var result = {
                    ruleWasViolated: !1
                },
                ruleIsApplicable = !0,
                doorSelection = inputData.doorSelections[doorIndex],
                frontSideDoorPanelId = doorSelection.panelSelections[panelIndex].doorPanelIdsByDoorSide.Front,
                frontSideDoorPanel = staticData.doorPanelsById[frontSideDoorPanelId],
                frontSideDoorPanelMaterial = staticData.materialsById[frontSideDoorPanel.details.materialId];
            return rule.applicableDoorPanelConfigurationIds.length > 0 && -1 == util.inArray(rule.applicableDoorPanelConfigurationIds, doorSelection.doorPanelConfigurationId) && (ruleIsApplicable = !1), rule.applicablePanelIndexes.length > 0 && -1 == util.inArray(rule.applicablePanelIndexes, panelIndex + 1) && (ruleIsApplicable = !1), "BackSidePanelMustMatchFrontSidePanelOrBeExceptionPanel" == rule.ruleType && ("Back" != doorSide && (ruleIsApplicable = !1), rule.applicableFrontSideDoorPanelIds.length > 0 && -1 == util.inArray(rule.applicableFrontSideDoorPanelIds, frontSideDoorPanelId) && (ruleIsApplicable = !1), rule.applicableFrontSideDoorPanelMaterialTypes.length > 0 && -1 == util.inArray(rule.applicableFrontSideDoorPanelMaterialTypes, frontSideDoorPanelMaterial.details.materialType) && (ruleIsApplicable = !1)), ruleIsApplicable && "BackSidePanelMustMatchFrontSidePanelOrBeExceptionPanel" == rule.ruleType && desiredDoorPanel.id != frontSideDoorPanelId && -1 == util.inArray(rule.exceptionDoorPanelIds, desiredDoorPanel.id) && -1 == util.inArray(rule.exceptionDoorPanelIds, frontSideDoorPanelId) && (result.ruleWasViolated = !0, result.reason = "DoorPanelMustMatchFrontSideDoorPanelOrBeExceptionDoorPanel", result.availableDoorPanelIds = [frontSideDoorPanelId].concat(rule.exceptionDoorPanelIds)), result
        },
        checkIfDoorsAreTooWideForDesiredDoorPanel: function(desiredDoorPanelId) {
            var desiredDoorPanel = staticData.doorPanelsById[desiredDoorPanelId],
                doorsAreTooWide = null != desiredDoorPanel.details.maximumDoorWidth && calculatedData.physicalMetrics.requiredDoorWidth > desiredDoorPanel.details.maximumDoorWidth;
            return doorsAreTooWide
        },
        checkIfDoorPanelConfigurationIsAllowedForDesiredDoorPanel: function(desiredDoorPanelId, doorIndex) {
            var doorPanelConfigurationIsAllowed = !0,
                doorPanelConfigurationId = inputData.doorSelections[doorIndex].doorPanelConfigurationId,
                doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId],
                desiredDoorPanel = staticData.doorPanelsById[desiredDoorPanelId];
            return desiredDoorPanel.details.canBeUsedWithAnyDoorPanelConfiguration || (doorPanelConfigurationIsAllowed = -1 != util.inArray(desiredDoorPanel.details.allowedDoorPanelConfigurationIds, doorPanelConfigurationId)), null == doorRange.allowedDoorPanelIdsLookup[desiredDoorPanelId] && (doorPanelConfigurationIsAllowed &= conditionalDoorPanelSetConditionChecker.checkIfDoorPanelAndDoorPanelConfigurationCombinationIsAllowedByConditionalDoorPanelSets(desiredDoorPanelId, doorPanelConfigurationId)), doorPanelConfigurationIsAllowed
        }
    }
}]), siteApp.factory("doorPanelPlacementValidityBuilder", ["doorPanelPlacementConstraintsBuilder", "staticData", "inputData", "util", function(doorPanelPlacementConstraintsBuilder) {
    return {
        buildDoorPanelPlacementValidity: function() {
            var doorPanelPlacementValidity = {
                    misplacedDoorPanelsLookup: {},
                    validityHintsByDoorSide: {
                        Front: {
                            oneOrMorePanelSelectionsAreInvalid: !1,
                            oneOrMorePanelSelectionsAreMissing: !1
                        },
                        Back: {
                            oneOrMorePanelSelectionsAreInvalid: !1,
                            oneOrMorePanelSelectionsAreMissing: !1
                        }
                    }
                },
                placementConstraintsLookup = doorPanelPlacementConstraintsBuilder.buildPlacementConstraintsLookup(null);
            for (var key in placementConstraintsLookup) {
                var doorPanelPlacementConstraints = placementConstraintsLookup[key];
                doorPanelPlacementConstraints.desiredDoorPanelCanBeApplied || (doorPanelPlacementValidity.misplacedDoorPanelsLookup[key] = !0, "DoorPanelSelectionIsRequiredInThisLocation" == doorPanelPlacementConstraints.reason ? doorPanelPlacementValidity.validityHintsByDoorSide[doorPanelPlacementConstraints.doorSide].oneOrMorePanelSelectionsAreMissing = !0 : doorPanelPlacementValidity.validityHintsByDoorSide[doorPanelPlacementConstraints.doorSide].oneOrMorePanelSelectionsAreInvalid = !0)
            }
            return doorPanelPlacementValidity
        }
    }
}]), siteApp.factory("doorPlacementPlanManager", ["staticData", "inputData", "util", function(staticData, inputData) {
    return {
        determineIfDoorPlacementPlanIsSuitable: function(doorPlacementPlan, numberOfDoors) {
            var doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId],
                userCanSelectOverlappingModeForThisDoorRange = doorRange.allowUserToSelectOverlappingMode || staticData.permissions.canSelectDoorOverlappingMode;
            return (userCanSelectOverlappingModeForThisDoorRange || doorPlacementPlan.overlappingMode == this.doorPlacementPlanSetsByNumberOfDoors[numberOfDoors].defaultOverlappingMode) && doorPlacementPlan.allowedByConfiguratorMode[staticData.mode] ? !0 : !1
        },
        identifySuitableDoorPlacementPlan: function() {
            var numberOfDoors = inputData.doorOptions.numberOfDoors;
            1 > numberOfDoors && (numberOfDoors = 1);
            var doorPlacementPlanSet = this.doorPlacementPlanSetsByNumberOfDoors[numberOfDoors],
                doorPlacementPlan = doorPlacementPlanSet.doorPlacementPlansByOverlappingMode[inputData.doorOptions.doorOverlappingMode];
            return null == doorPlacementPlan && (doorPlacementPlan = doorPlacementPlanSet.doorPlacementPlansByOverlappingMode.Interleaving), doorPlacementPlan
        },
        getDoorPlacementPlan: function(numberOfDoors, overlappingMode) {
            return this.doorPlacementPlanSetsByNumberOfDoors[numberOfDoors].doorPlacementPlansByOverlappingMode[overlappingMode]
        },
        doorPlacementPlanSetsByNumberOfDoors: {
            1: {
                defaultOverlappingMode: "Interleaving",
                doorPlacementPlansByOverlappingMode: {
                    Interleaving: {
                        overlappingMode: "Interleaving",
                        allowedByConfiguratorMode: {
                            Wardrobe: !0,
                            RoomDivider: !0
                        },
                        totalOverlaps: 0,
                        numberOfTracks: 1,
                        doorPlacementsByDoorNumber: {
                            0: {
                                trackNumber: 0,
                                leftOverlap: !1
                            }
                        }
                    }
                }
            },
            2: {
                defaultOverlappingMode: "Interleaving",
                doorPlacementPlansByOverlappingMode: {
                    MeetInTheMiddle: {
                        overlappingMode: "MeetInTheMiddle",
                        allowedByConfiguratorMode: {
                            Wardrobe: !1,
                            RoomDivider: !0
                        },
                        totalOverlaps: 0,
                        numberOfTracks: 1,
                        doorPlacementsByDoorNumber: {
                            0: {
                                trackNumber: 0,
                                leftOverlap: !1
                            },
                            1: {
                                trackNumber: 0,
                                leftOverlap: !1
                            }
                        }
                    },
                    Interleaving: {
                        overlappingMode: "Interleaving",
                        allowedByConfiguratorMode: {
                            Wardrobe: !0,
                            RoomDivider: !0
                        },
                        totalOverlaps: 1,
                        numberOfTracks: 2,
                        doorPlacementsByDoorNumber: {
                            0: {
                                trackNumber: 1,
                                leftOverlap: !1
                            },
                            1: {
                                trackNumber: 0,
                                leftOverlap: !0
                            }
                        }
                    }
                }
            },
            3: {
                defaultOverlappingMode: "Interleaving",
                doorPlacementPlansByOverlappingMode: {
                    Interleaving: {
                        overlappingMode: "Interleaving",
                        allowedByConfiguratorMode: {
                            Wardrobe: !0,
                            RoomDivider: !0
                        },
                        totalOverlaps: 2,
                        numberOfTracks: 2,
                        doorPlacementsByDoorNumber: {
                            0: {
                                trackNumber: 1,
                                leftOverlap: !1
                            },
                            1: {
                                trackNumber: 0,
                                leftOverlap: !0
                            },
                            2: {
                                trackNumber: 1,
                                leftOverlap: !0
                            }
                        }
                    }
                }
            },
            4: {
                defaultOverlappingMode: "MeetInTheMiddle",
                doorPlacementPlansByOverlappingMode: {
                    MeetInTheMiddle: {
                        overlappingMode: "MeetInTheMiddle",
                        allowedByConfiguratorMode: {
                            Wardrobe: !0,
                            RoomDivider: !0
                        },
                        totalOverlaps: 2,
                        numberOfTracks: 2,
                        doorPlacementsByDoorNumber: {
                            0: {
                                trackNumber: 1,
                                leftOverlap: !1
                            },
                            1: {
                                trackNumber: 0,
                                leftOverlap: !0
                            },
                            2: {
                                trackNumber: 0,
                                leftOverlap: !1
                            },
                            3: {
                                trackNumber: 1,
                                leftOverlap: !0
                            }
                        }
                    },
                    Interleaving: {
                        overlappingMode: "Interleaving",
                        allowedByConfiguratorMode: {
                            Wardrobe: !0,
                            RoomDivider: !0
                        },
                        totalOverlaps: 3,
                        numberOfTracks: 2,
                        doorPlacementsByDoorNumber: {
                            0: {
                                trackNumber: 1,
                                leftOverlap: !1
                            },
                            1: {
                                trackNumber: 0,
                                leftOverlap: !0
                            },
                            2: {
                                trackNumber: 1,
                                leftOverlap: !0
                            },
                            3: {
                                trackNumber: 0,
                                leftOverlap: !0
                            }
                        }
                    }
                }
            },
            5: {
                defaultOverlappingMode: "Interleaving",
                doorPlacementPlansByOverlappingMode: {
                    Interleaving: {
                        overlappingMode: "Interleaving",
                        allowedByConfiguratorMode: {
                            Wardrobe: !0,
                            RoomDivider: !0
                        },
                        totalOverlaps: 4,
                        numberOfTracks: 2,
                        doorPlacementsByDoorNumber: {
                            0: {
                                trackNumber: 1,
                                leftOverlap: !1
                            },
                            1: {
                                trackNumber: 0,
                                leftOverlap: !0
                            },
                            2: {
                                trackNumber: 1,
                                leftOverlap: !0
                            },
                            3: {
                                trackNumber: 0,
                                leftOverlap: !0
                            },
                            4: {
                                trackNumber: 1,
                                leftOverlap: !0
                            }
                        }
                    }
                }
            },
            6: {
                defaultOverlappingMode: "MeetInTheMiddle",
                doorPlacementPlansByOverlappingMode: {
                    MeetInTheMiddle: {
                        overlappingMode: "MeetInTheMiddle",
                        allowedByConfiguratorMode: {
                            Wardrobe: !0,
                            RoomDivider: !0
                        },
                        totalOverlaps: 4,
                        numberOfTracks: 2,
                        doorPlacementsByDoorNumber: {
                            0: {
                                trackNumber: 0,
                                leftOverlap: !1
                            },
                            1: {
                                trackNumber: 1,
                                leftOverlap: !0
                            },
                            2: {
                                trackNumber: 0,
                                leftOverlap: !0
                            },
                            3: {
                                trackNumber: 0,
                                leftOverlap: !1
                            },
                            4: {
                                trackNumber: 1,
                                leftOverlap: !0
                            },
                            5: {
                                trackNumber: 0,
                                leftOverlap: !0
                            }
                        }
                    },
                    Interleaving: {
                        overlappingMode: "Interleaving",
                        allowedByConfiguratorMode: {
                            Wardrobe: !0,
                            RoomDivider: !0
                        },
                        totalOverlaps: 5,
                        numberOfTracks: 2,
                        doorPlacementsByDoorNumber: {
                            0: {
                                trackNumber: 0,
                                leftOverlap: !1
                            },
                            1: {
                                trackNumber: 1,
                                leftOverlap: !0
                            },
                            2: {
                                trackNumber: 0,
                                leftOverlap: !0
                            },
                            3: {
                                trackNumber: 1,
                                leftOverlap: !0
                            },
                            4: {
                                trackNumber: 0,
                                leftOverlap: !0
                            },
                            5: {
                                trackNumber: 1,
                                leftOverlap: !0
                            }
                        }
                    }
                }
            }
        }
    }
}]), siteApp.factory("doorQuantityRulesCalculator", ["staticData", "inputData", "doorPlacementPlanManager", "apertureSizeCalculator", "physicalMetricsCalculator", "doorLinerQuantityCalculator", function(staticData, inputData, doorPlacementPlanManager, apertureSizeCalculator, physicalMetricsCalculator, doorLinerQuantityCalculator) {
    return {
        calculateDoorQuantityRules: function() {
            for (var newDoorQuantityRules = {
                    minNumberOfDoors: -1,
                    maxNumberOfDoors: -1,
                    doorQuantityDetailsByNumberOfDoors: {}
                }, doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], absoluteMinimumNumberOfDoors = doorRange.doorQuantityRules.minimumNumberOfDoors, absoluteMaximumNumberOfDoors = doorRange.doorQuantityRules.maximumNumberOfDoors, numberOfDoors = absoluteMinimumNumberOfDoors; absoluteMaximumNumberOfDoors >= numberOfDoors; numberOfDoors++) {
                var doorQuantityDetails = this.calculateDoorQuantityDetails(numberOfDoors);
                doorQuantityDetails.allowed && (-1 == newDoorQuantityRules.minNumberOfDoors && (newDoorQuantityRules.minNumberOfDoors = numberOfDoors), (-1 == newDoorQuantityRules.maxNumberOfDoors || newDoorQuantityRules.maxNumberOfDoors < numberOfDoors) && (newDoorQuantityRules.maxNumberOfDoors = numberOfDoors)), newDoorQuantityRules.doorQuantityDetailsByNumberOfDoors[numberOfDoors] = doorQuantityDetails
            }
            return -1 == newDoorQuantityRules.minNumberOfDoors && (newDoorQuantityRules.minNumberOfDoors = absoluteMinimumNumberOfDoors), -1 == newDoorQuantityRules.maxNumberOfDoors && (newDoorQuantityRules.maxNumberOfDoors = absoluteMinimumNumberOfDoors), newDoorQuantityRules
        },
        calculateDoorQuantityDetails: function(numberOfDoors) {
            for (var doorQuantityDetails = {
                    numberOfDoors: numberOfDoors,
                    allowed: !1,
                    allowedOverlappingModes: []
                }, doorSizeRules = this.getDoorSizeRules(), doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], requiredDoorLinerQuantities = doorLinerQuantityCalculator.calculateRequiredDoorLinerQuantities(inputData.doorLinerOptions.selectionMode, numberOfDoors), apertureSize = apertureSizeCalculator.calculateApertureSize(requiredDoorLinerQuantities), i = 0; i < staticData.doorOverlappingModes.length; i++) {
                var overlappingMode = staticData.doorOverlappingModes[i].mode,
                    doorPlacementPlan = doorPlacementPlanManager.getDoorPlacementPlan(numberOfDoors, overlappingMode);
                if (null != doorPlacementPlan && doorPlacementPlanManager.determineIfDoorPlacementPlanIsSuitable(doorPlacementPlan, numberOfDoors)) {
                    var doorWidth = physicalMetricsCalculator.calculateDoorWidth(apertureSize, doorPlacementPlan.totalOverlaps, doorRange.doorOverlapWidth, numberOfDoors);
                    doorWidth >= doorSizeRules.minimumDoorWidth && doorWidth <= doorSizeRules.maximumDoorWidth && (doorQuantityDetails.allowed = !0, doorQuantityDetails.allowedOverlappingModes.push(overlappingMode))
                }
            }
            return doorQuantityDetails
        },
        getDoorSizeRules: function() {
            var doorSizeRules = staticData.doorRangesById[inputData.doorOptions.doorRangeId].doorSizeRules;
            return null == doorSizeRules && (doorSizeRules = staticData.defaultDoorSizeRules), doorSizeRules
        }
    }
}]), siteApp.factory("doorSelectionSanitiser", ["doorPanelPlacementAutomationRuleManager", "doorPanelHelper", "staticData", "inputData", "util", function(doorPanelPlacementAutomationRuleManager, doorPanelHelper, staticData, inputData, util) {
    return {
        sanitiseDoorSelections: function() {
            for (var selectedDoorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], doorIndex = 0; doorIndex < inputData.doorOptions.numberOfDoors; doorIndex++) {
                var doorSelection = inputData.doorSelections[doorIndex],
                    previousDoorSelection = inputData.doorSelections[doorIndex - 1];
                null == doorSelection && (doorSelection = {
                    doorPanelConfigurationId: null,
                    panelSelections: null
                }, null != previousDoorSelection && angular.copy(previousDoorSelection, doorSelection), inputData.doorSelections.push(doorSelection)), (null == doorSelection.doorPanelConfigurationId || null == staticData.doorPanelConfigurationsById[doorSelection.doorPanelConfigurationId]) && (doorSelection.doorPanelConfigurationId = selectedDoorRange.allowedDoorPanelConfigurationIds[0]), -1 == util.inArray(selectedDoorRange.allowedDoorPanelConfigurationIds, doorSelection.doorPanelConfigurationId) && (doorSelection.doorPanelConfigurationWasUserSelected || (doorSelection.doorPanelConfigurationId = selectedDoorRange.allowedDoorPanelConfigurationIds[0])), null == doorSelection.panelSelections && (doorSelection.panelSelections = []);
                for (var selectedDoorPanelConfiguration = staticData.doorPanelConfigurationsById[doorSelection.doorPanelConfigurationId], numberOfPanels = selectedDoorPanelConfiguration.details.numberOfPanels, panelIndex = 0; numberOfPanels > panelIndex; panelIndex++) {
                    var panelSelection = doorSelection.panelSelections[panelIndex],
                        previousPanelSelection = doorSelection.panelSelections[panelIndex - 1];
                    null == panelSelection && (panelSelection = {
                        doorPanelIdsByDoorSide: null,
                        frontSideWasUserSelected: !1,
                        backSideWasUserSelected: !1
                    }, doorSelection.panelSelections.push(panelSelection)), null == panelSelection.doorPanelIdsByDoorSide && (panelSelection.doorPanelIdsByDoorSide = {}), this.sanitiseFrontSideOfPanelSelection(panelSelection, previousPanelSelection), "RoomDivider" == inputData.mode && this.sanitiseBackSideOfPanelSelection(panelSelection, previousPanelSelection);
                    var applicableDoorPanelPlacementAutomationRules = doorPanelPlacementAutomationRuleManager.findApplicableDoorPanelPlacementAutomationRulesForPanelSelection(doorIndex, panelIndex);
                    doorPanelPlacementAutomationRuleManager.applyDoorPanelPlacementAutomationRulesToPanelSelection(applicableDoorPanelPlacementAutomationRules, doorIndex, panelIndex)
                }
                util.cropArray(doorSelection.panelSelections, numberOfPanels)
            }
            util.cropArray(inputData.doorSelections, inputData.doorOptions.numberOfDoors)
        },
        sanitiseFrontSideOfPanelSelection: function(panelSelection, previousPanelSelection) {
            var defaultFrontSideDoorPanelId = staticData.doorRangesById[inputData.doorOptions.doorRangeId].defaultFrontSideDoorPanelId;
            inputData.doorOptions.atLeastOneDoorPanelWasUserSelected ? null == panelSelection.doorPanelIdsByDoorSide.Front && (null == previousPanelSelection ? (panelSelection.doorPanelIdsByDoorSide.Front = defaultFrontSideDoorPanelId, panelSelection.frontSideWasUserSelected = !1) : (panelSelection.doorPanelIdsByDoorSide.Front = previousPanelSelection.doorPanelIdsByDoorSide.Front, panelSelection.frontSideWasUserSelected = previousPanelSelection.frontSideWasUserSelected)) : panelSelection.doorPanelIdsByDoorSide.Front = defaultFrontSideDoorPanelId
        },
        sanitiseBackSideOfPanelSelection: function(panelSelection, previousPanelSelection) {
            var doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId],
                previousBackSideDoorPanel = null != previousPanelSelection ? staticData.doorPanelsById[previousPanelSelection.doorPanelIdsByDoorSide.Back] : null,
                currentFrontSideDoorPanel = staticData.doorPanelsById[panelSelection.doorPanelIdsByDoorSide.Front],
                currentBackSideDoorPanel = staticData.doorPanelsById[panelSelection.doorPanelIdsByDoorSide.Back],
                currentFrontSideDoorPanelSidedness = doorPanelHelper.getDoorPanelEffectiveSidedness(currentFrontSideDoorPanel),
                currentBackSideDoorPanelSidedness = null != currentBackSideDoorPanel ? doorPanelHelper.getDoorPanelEffectiveSidedness(currentBackSideDoorPanel) : null,
                previousBackSideDoorPanelSidedness = null != previousBackSideDoorPanel ? doorPanelHelper.getDoorPanelEffectiveSidedness(previousBackSideDoorPanel) : null;
            if ("DoubleSided" == currentFrontSideDoorPanelSidedness) panelSelection.doorPanelIdsByDoorSide.Back = null, panelSelection.backSideWasUserSelected = !1;
            else if ("SingleSidedThick" == currentFrontSideDoorPanelSidedness) {
                var currentBackSideDoorPanelIsSuitable = null != currentBackSideDoorPanel && "SingleSided" == currentBackSideDoorPanelSidedness;
                if (!currentBackSideDoorPanelIsSuitable && !panelSelection.backSideWasUserSelected) {
                    var previousBackSideDoorPanelIsSuitable = null != previousBackSideDoorPanel && "SingleSided" == previousBackSideDoorPanelSidedness,
                        replacementDoorPanelId = previousBackSideDoorPanelIsSuitable ? previousBackSideDoorPanel.id : doorRange.defaultBackSideDoorPanelId;
                    panelSelection.doorPanelIdsByDoorSide.Back = replacementDoorPanelId
                }
            } else if ("SingleSided" == currentFrontSideDoorPanelSidedness) {
                var currentBackSideDoorPanelIsSuitable = null != currentBackSideDoorPanel && "DoubleSided" != currentBackSideDoorPanelSidedness;
                if (!currentBackSideDoorPanelIsSuitable && !panelSelection.backSideWasUserSelected) {
                    var previousBackSideDoorPanelIsSuitable = null != previousBackSideDoorPanel && "DoubleSided" != previousBackSideDoorPanelSidedness,
                        replacementDoorPanelId = previousBackSideDoorPanelIsSuitable ? previousBackSideDoorPanel.id : currentFrontSideDoorPanel.id || doorRange.defaultBackSideDoorPanelId;
                    panelSelection.doorPanelIdsByDoorSide.Back = replacementDoorPanelId
                }
            }
        }
    }
}]), siteApp.factory("inputData", [function() {
    return {}
}]), siteApp.factory("installationDetails", [function() {
    return {}
}]), siteApp.factory("materialHelper", ["staticData", "util", function(staticData, util) {
    return {
        coalesceMaterial: function(materialId, allowedMaterialIds) {
            return -1 == util.inArray(allowedMaterialIds, materialId) && (materialId = staticData.materialsById[materialId].details.similarMaterialId, -1 == util.inArray(allowedMaterialIds, materialId) && (materialId = null)), null == materialId && (materialId = allowedMaterialIds[0]), materialId
        }
    }
}]), siteApp.factory("openingSizeRulesHelper", ["staticData", "inputData", function(staticData, inputData) {
    return {
        getOpeningSizeRules: function() {
            var openingSizeRules = staticData.doorRangesById[inputData.doorOptions.doorRangeId].openingSizeRules;
            return null == openingSizeRules && (openingSizeRules = staticData.defaultOpeningSizeRules), openingSizeRules
        }
    }
}]), siteApp.factory("physicalMetricsCalculator", ["staticData", "inputData", "doorPlacementPlanManager", "apertureSizeCalculator", "conditionalDoorRangeConstructionCoefficientsResolver", "settings", function(staticData, inputData, doorPlacementPlanManager, apertureSizeCalculator, conditionalDoorRangeConstructionCoefficientsResolver, settings) {
    return {
        calculatePhysicalMetrics: function() {
            var physicalMetrics = {
                    totalSystemSize: {
                        width: 0,
                        height: 0
                    },
                    aperturePosition: {
                        x: 0,
                        y: 0
                    },
                    apertureSize: {
                        width: 0,
                        height: 0
                    },
                    numberOfDoorOverlaps: 0,
                    doorOverlapWidth: 0,
                    numberOfTracks: 0
                },
                doorRangeConstructionCoefficients = conditionalDoorRangeConstructionCoefficientsResolver.findApplicableDoorRangeConstructionCoefficients(),
                doorPlacementPlan = doorPlacementPlanManager.identifySuitableDoorPlacementPlan(),
                doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId];
            return physicalMetrics.totalSystemSize.width = inputData.openingSize.width, physicalMetrics.totalSystemSize.height = inputData.openingSize.height, inputData.endPanelOptions.hasLeftEndPanel && (physicalMetrics.aperturePosition.x += staticData.endPanelThickness), physicalMetrics.aperturePosition.x += inputData.doorLinerOptions.quantitiesByPlacementLocation.Left * staticData.doorLinerThickness, physicalMetrics.aperturePosition.y = physicalMetrics.totalSystemSize.height - inputData.doorLinerOptions.quantitiesByPlacementLocation.Top * staticData.doorLinerThickness, physicalMetrics.apertureSize = apertureSizeCalculator.calculateApertureSize(), physicalMetrics.numberOfDoorOverlaps = doorPlacementPlan.totalOverlaps, physicalMetrics.doorOverlapWidth = doorPlacementPlan.totalOverlaps > 0 ? doorRange.doorOverlapWidth : 0, physicalMetrics.requiredDoorWidth = this.calculateDoorWidth(physicalMetrics.apertureSize, physicalMetrics.numberOfDoorOverlaps, doorRange.doorOverlapWidth, inputData.doorOptions.numberOfDoors), physicalMetrics.requiredDoorHeight = this.calculateDoorHeight(physicalMetrics.apertureSize, doorRangeConstructionCoefficients), physicalMetrics.numberOfTracks = doorPlacementPlan.numberOfTracks, physicalMetrics.doorsAreTooWideForInstallation = null != settings.maximumAllowedDoorWidthForInstallation && physicalMetrics.requiredDoorWidth > settings.maximumAllowedDoorWidthForInstallation, physicalMetrics
        },
        calculateDoorWidth: function(apertureSize, numberOfOverlaps, doorOverlapWidth, numberOfDoors) {
            return (apertureSize.width + numberOfOverlaps * doorOverlapWidth) / numberOfDoors
        },
        calculateDoorHeight: function(apertureSize, doorRangeConstructionCoefficients) {
            return apertureSize.height - doorRangeConstructionCoefficients.doorHeightAllowance
        }
    }
}]), siteApp.factory("previewRasteriser", ["renderingImageManager", function(renderingImageManager) {
    return {
        onScreenCanvas: null,
        onScreenContext: null,
        init: function() {},
        executeRasterJob: function(rasterJob) {
            if (null == this.onScreenCanvas || this.onScreenContext) {
                if (this.onScreenCanvas = document.getElementById("preview-canvas"), null == this.onScreenCanvas) return;
                this.onScreenContext = this.onScreenCanvas.getContext("2d")
            }
            for (var canvas = this.onScreenCanvas, context = this.onScreenContext, i = 0, ri = function() {
                    return rasterJob.instructions[i++]
                }; i < rasterJob.instructions.length;) {
                var inst = ri();
                if (inst == RasterInstructionSymbols.clear) context.clearRect(0, 0, canvas.width, canvas.height);
                else if (inst == RasterInstructionSymbols.rect) context.strokeStyle = ri(), context.strokeRect(ri(), ri(), ri(), ri());
                else if (inst == RasterInstructionSymbols.fillRect) context.fillStyle = ri(), context.fillRect(ri(), ri(), ri(), ri());
                else if (inst == RasterInstructionSymbols.fillText) {
                    var rotationRadians = ri();
                    context.rotate(rotationRadians), context.fillStyle = ri(), context.font = ri(), context.fillText(ri(), ri(), ri()), context.rotate(-rotationRadians)
                } else if (inst == RasterInstructionSymbols.drawClippedImage) {
                    var imageId = this.deindexifyImageId(rasterJob, ri()),
                        image = renderingImageManager.findImage(imageId);
                    null == image && (image = renderingImageManager.findImage("missing-image"));
                    var clipOffsetX = ri(),
                        clipOffsetY = ri(),
                        clipWidth = ri(),
                        clipHeight = ri();
                    if (clipOffsetX + clipWidth > image.naturalWidth) {
                        var remainingWidth = clipOffsetX + clipWidth - image.naturalWidth;
                        clipWidth -= remainingWidth
                    }
                    if (clipOffsetY + clipHeight > image.naturalHeight) {
                        var remainingHeight = clipOffsetY + clipHeight - image.naturalHeight;
                        clipHeight -= remainingHeight
                    }
                    var x = ri(),
                        y = ri(),
                        width = ri(),
                        height = ri();
                    context.drawImage(image, clipOffsetX, clipOffsetY, clipWidth, clipHeight, x, y, width, height)
                } else if (inst == RasterInstructionSymbols.drawImage) {
                    var imageId = this.deindexifyImageId(rasterJob, ri()),
                        image = renderingImageManager.findImage(imageId);
                    context.drawImage(image, ri(), ri(), ri(), ri())
                }
            }
        },
        deindexifyImageId: function(rasterJob, imageIdIndex) {
            return rasterJob.imageIds[imageIdIndex]
        }
    }
}]), siteApp.factory("previewDoorsRenderer", ["doorPanelHelper", "previewSettings", "staticData", "inputData", "calculatedData", "translator", "util", function(doorPanelHelper, previewSettings, staticData, inputData, calculatedData, translator) {
    return {
        job: null,
        screenSpaceTransform: null,
        hotspots: null,
        init: function() {},
        renderDoors: function(job, screenSpaceTransform, hotspots) {
            this.job = job, this.screenSpaceTransform = screenSpaceTransform, this.hotspots = hotspots;
            var endPanelOptions = inputData.endPanelOptions,
                physicalMetrics = calculatedData.physicalMetrics,
                renderMetrics = calculatedData.renderMetrics;
            endPanelOptions.hasLeftEndPanel && this.drawEndPanel(renderMetrics.leftEndPanel);
            for (var i = 0; i < renderMetrics.doorLiners.length; i++) {
                var doorLinerMetrics = renderMetrics.doorLiners[i];
                this.drawDoorLiner(doorLinerMetrics)
            }
            for (var trackNumber = "Front" == previewSettings.lookAtDoorSide ? physicalMetrics.numberOfTracks - 1 : 0; trackNumber >= 0 && trackNumber < physicalMetrics.numberOfTracks; trackNumber += "Front" == previewSettings.lookAtDoorSide ? -1 : 1)
                for (var doorIndex = 0; doorIndex < renderMetrics.doors.length; doorIndex++) {
                    var doorMetrics = renderMetrics.doors[doorIndex];
                    doorMetrics.trackNumber == trackNumber && this.drawDoor(doorIndex)
                }
            endPanelOptions.hasRightEndPanel && this.drawEndPanel(renderMetrics.rightEndPanel)
        },
        drawDoor: function(doorIndex) {
            for (var renderMetrics = calculatedData.renderMetrics, doorMetrics = renderMetrics.doors[doorIndex], panelIndex = 0; panelIndex < doorMetrics.panels.length; panelIndex++) this.drawDoorPanel(doorIndex, panelIndex);
            for (var frameIndex = 0; frameIndex < doorMetrics.frames.length; frameIndex++) this.drawDoorFrame(doorIndex, frameIndex);
            for (var handleIndex = 0; handleIndex < doorMetrics.handles.length; handleIndex++) this.drawDoorHandle(doorIndex, handleIndex);
            this.drawDoorEdgeShadows(doorIndex), "RoomDivider" == inputData.mode && this.drawDoorLabel(doorIndex)
        },
        drawEndPanel: function(endPanelMetrics) {
            var endPanelOptions = inputData.endPanelOptions,
                material = staticData.materialsById[endPanelOptions.materialId];
            this.drawWorldSpaceTexturedRect({
                x: endPanelMetrics.x,
                y: endPanelMetrics.y,
                width: endPanelMetrics.width,
                height: endPanelMetrics.height,
                imageId: material.details.textureImageMediaItemIdsByOrientation.Horizontal,
                fallbackColour: material.details.colour,
                bevels: {
                    top: !0,
                    bottom: !0,
                    left: !0,
                    right: !0
                }
            })
        },
        drawDoorLiner: function(doorLinerMetrics) {
            var doorLinerOptions = inputData.doorLinerOptions,
                material = staticData.materialsById[doorLinerOptions.materialId];
            this.drawWorldSpaceTexturedRect({
                x: doorLinerMetrics.x,
                y: doorLinerMetrics.y,
                width: doorLinerMetrics.width,
                height: doorLinerMetrics.height,
                imageId: material.details.textureImageMediaItemIdsByOrientation.Horizontal,
                fallbackColour: material.details.colour,
                bevels: {
                    top: !0,
                    bottom: !0,
                    left: !0,
                    right: !0
                }
            })
        },
        drawDoorPanel: function(doorIndex, panelIndex) {
            var doorSelection = inputData.doorSelections[doorIndex],
                panelSelection = doorSelection.panelSelections[panelIndex],
                panelMetrics = calculatedData.renderMetrics.doors[doorIndex].panels[panelIndex],
                doorPanelIdToRender = null;
            if ("Front" == previewSettings.lookAtDoorSide) doorPanelIdToRender = panelSelection.doorPanelIdsByDoorSide.Front;
            else if ("Back" == previewSettings.lookAtDoorSide) {
                var frontDoorPanelId = panelSelection.doorPanelIdsByDoorSide.Front,
                    frontDoorPanel = staticData.doorPanelsById[frontDoorPanelId],
                    frontDoorPanelSidedness = doorPanelHelper.getDoorPanelEffectiveSidedness(frontDoorPanel);
                doorPanelIdToRender = "DoubleSided" == frontDoorPanelSidedness ? frontDoorPanelId : panelSelection.doorPanelIdsByDoorSide.Back
            }
            var panelKey = "doorIndex:" + doorIndex + "-panelIndex:" + panelIndex + "-doorSide:" + previewSettings.lookAtDoorSide,
                doorPanelPlacementConstraint = calculatedData.placementConstraintsForSelectedDoorPanelLookup[panelKey],
                doorPanelIsEditable = doorPanelPlacementConstraint.desiredDoorPanelCanBeApplied,
                textureImageMediaItemId = "missing-image",
                doorPanelToRender = staticData.doorPanelsById[doorPanelIdToRender],
                doorPanelConfiguration = staticData.doorPanelConfigurationsById[doorSelection.doorPanelConfigurationId];
            if (null != doorPanelToRender) {
                var doorPanelUsageDetails = doorPanelToRender.details.usageDetailsByUsageType[doorPanelConfiguration.details.doorPanelUsageType],
                    material = staticData.materialsById[doorPanelToRender.details.materialId];
                textureImageMediaItemId = material.details.textureImageMediaItemIdsByOrientation[doorPanelUsageDetails.materialTextureOrientation]
            }
            var doorPanelIsMisplaced = calculatedData.validity.misplacedDoorPanelsLookup[panelKey],
                hotspotMarkerType = "";
            doorPanelIsMisplaced ? hotspotMarkerType = "error" : previewSettings.enableEditMode && (hotspotMarkerType = doorPanelIsEditable ? "pulsing-hand" : "access-denied");
            var panelClickHandler = function() {
                previewSettings.enableEditMode && previewSettings.doorPanelClickHandler(doorIndex, panelIndex, previewSettings.lookAtDoorSide)
            };
            this.drawWorldSpaceTexturedRect({
                x: panelMetrics.x,
                y: panelMetrics.y,
                width: panelMetrics.width,
                height: panelMetrics.height,
                imageId: textureImageMediaItemId,
                fallbackColour: "#ff0000",
                hotspot: {
                    markerType: hotspotMarkerType,
                    onClick: panelClickHandler
                }
            })
        },
        drawDoorFrame: function(doorIndex, frameIndex) {
            var material = staticData.materialsById[inputData.doorOptions.doorFrameMaterialId],
                textureImageMediaItemId = material.details.textureImageMediaItemIdsByOrientation.Horizontal,
                frameMetrics = calculatedData.renderMetrics.doors[doorIndex].frames[frameIndex];
            this.drawWorldSpaceTexturedRect({
                x: frameMetrics.x,
                y: frameMetrics.y,
                width: frameMetrics.width,
                height: frameMetrics.height,
                imageId: textureImageMediaItemId,
                fallbackColour: material.details.colour,
                bevels: frameMetrics.bevels
            })
        },
        drawDoorHandle: function(doorIndex, handleIndex) {
            var handleMetrics = calculatedData.renderMetrics.doors[doorIndex].handles[handleIndex],
                material = staticData.materialsById[inputData.doorOptions.doorFrameMaterialId],
                textureImageMediaItemId = material.details.textureImageMediaItemIdsByOrientation.Horizontal;
            this.drawWorldSpaceImage({
                x: handleMetrics.x,
                y: handleMetrics.y,
                width: handleMetrics.width,
                height: handleMetrics.height,
                imageId: textureImageMediaItemId
            }), "FrameHandle" == handleMetrics.handleType ? this.drawWorldSpaceImage({
                x: handleMetrics.x,
                y: handleMetrics.y,
                width: handleMetrics.width,
                height: handleMetrics.height,
                imageId: "light-map-frame-handle"
            }) : "PanelHandle" == handleMetrics.handleType && this.drawWorldSpaceImage({
                x: handleMetrics.x,
                y: handleMetrics.y,
                width: handleMetrics.width,
                height: handleMetrics.height,
                imageId: "light-map-panel-handle"
            })
        },
        drawDoorOutline: function(doorMetrics) {
            var rect = {
                x: doorMetrics.x,
                y: doorMetrics.y,
                width: doorMetrics.width,
                height: doorMetrics.height
            };
            this.drawWorldSpaceRect(rect)
        },
        drawDoorEdgeShadows: function(doorIndex) {
            var renderMetrics = calculatedData.renderMetrics,
                doorMetrics = renderMetrics.doors[doorIndex],
                previousDoorMetrics = renderMetrics.doors[doorIndex - 1],
                nextDoorMetrics = renderMetrics.doors[doorIndex + 1],
                shadowWidth = 50;
            null != previousDoorMetrics && previousDoorMetrics.trackNumber != doorMetrics.trackNumber && this.drawWorldSpaceImage({
                x: doorMetrics.x - shadowWidth,
                y: doorMetrics.y,
                width: shadowWidth,
                height: doorMetrics.height,
                imageId: "Front" == previewSettings.lookAtDoorSide ? "door-shadow-left" : "door-shadow-right"
            }), null != nextDoorMetrics && nextDoorMetrics.trackNumber != doorMetrics.trackNumber && this.drawWorldSpaceImage({
                x: doorMetrics.x + doorMetrics.width,
                y: doorMetrics.y,
                width: shadowWidth,
                height: doorMetrics.height,
                imageId: "Front" == previewSettings.lookAtDoorSide ? "door-shadow-right" : "door-shadow-left"
            })
        },
        drawDoorLabel: function(doorIndex) {
            var renderMetrics = calculatedData.renderMetrics,
                doorMetrics = renderMetrics.doors[doorIndex],
                labelText = translator.translate("Door") + " " + (doorIndex + 1),
                labelWorldSpaceWidth = 45 / this.screenSpaceTransform.scaleFactor,
                labelWorldSpaceOffset = (doorMetrics.width - labelWorldSpaceWidth) / 2,
                labelWorldSpaceMargin = 5 / this.screenSpaceTransform.scaleFactor,
                labelX = 0;
            "Front" == previewSettings.lookAtDoorSide ? labelX = doorMetrics.x + labelWorldSpaceOffset : "Back" == previewSettings.lookAtDoorSide && (labelX = doorMetrics.x + labelWorldSpaceOffset + labelWorldSpaceWidth), this.drawWorldSpaceText({
                x: labelX,
                y: doorMetrics.y + labelWorldSpaceMargin,
                text: labelText,
                colour: "#888"
            })
        },
        drawWorldSpaceRect: function(options) {
            this.transformRectFromWorldSpaceToScreenSpace(options), this.job.rect(options.x, options.y, options.width, options.height, "#000000")
        },
        drawWorldSpaceTexturedRect: function(options) {
            var worldSpaceWidth = options.width,
                worldSpaceHeight = options.height;
            if (this.transformRectFromWorldSpaceToScreenSpace(options), null != options.imageId ? this.job.drawClippedImage(options.x, options.y, options.width, options.height, 0, 0, staticData.textureImageConfig.screenSpaceSize.width * (worldSpaceWidth / staticData.textureImageConfig.worldSpaceSize.width), staticData.textureImageConfig.screenSpaceSize.height * (worldSpaceHeight / staticData.textureImageConfig.worldSpaceSize.height), options.imageId) : this.job.fillRect(options.x, options.y, options.width, options.height, options.fallbackColour), null != options.bevels) {
                var bevelSize = 15;
                options.bevels.left && this.job.drawImage(options.x, options.y, bevelSize * this.screenSpaceTransform.scaleFactor, options.height, "light-map-bevel-left"), options.bevels.right && this.job.drawImage(options.x + options.width - bevelSize * this.screenSpaceTransform.scaleFactor, options.y, bevelSize * this.screenSpaceTransform.scaleFactor, options.height, "light-map-bevel-right"), options.bevels.top && this.job.drawImage(options.x, options.y, options.width, bevelSize * this.screenSpaceTransform.scaleFactor, "light-map-bevel-top"), options.bevels.bottom && this.job.drawImage(options.x, options.y + options.height - bevelSize * this.screenSpaceTransform.scaleFactor, options.width, bevelSize * this.screenSpaceTransform.scaleFactor, "light-map-bevel-bottom")
            }
            if (null != options.hotspot) {
                var hotspot = {
                    x: options.x,
                    y: options.y,
                    width: options.width,
                    height: options.height,
                    onClick: options.hotspot.onClick,
                    markerType: options.hotspot.markerType
                };
                this.hotspots.push(hotspot)
            }
        },
        drawWorldSpaceImage: function(options) {
            this.transformRectFromWorldSpaceToScreenSpace(options), this.job.drawImage(options.x, options.y, options.width, options.height, options.imageId)
        },
        drawWorldSpaceText: function(options) {
            this.transformPointFromWorldSpaceToScreenSpace(options), this.job.fillText(options.x, options.y, options.text, "14px arial", options.colour, 0)
        },
        transformRectFromWorldSpaceToScreenSpace: function(rect) {
            for (var points = [{
                    x: rect.x,
                    y: rect.y
                }, {
                    x: rect.x + rect.width,
                    y: rect.y - rect.height
                }], minX = null, minY = null, maxX = null, maxY = null, i = 0; i < points.length; i++) {
                var point = points[i];
                this.transformPointFromWorldSpaceToScreenSpace(point), (null == minX || point.x < minX) && (minX = point.x), (null == minY || point.y < minY) && (minY = point.y), (null == maxX || point.x > maxX) && (maxX = point.x), (null == maxY || point.y > maxY) && (maxY = point.y)
            }
            rect.x = minX, rect.y = minY, rect.width = maxX - minX, rect.height = maxY - minY
        },
        transformPointFromWorldSpaceToScreenSpace: function(p) {
            var physicalMetrics = calculatedData.physicalMetrics;
            "Back" == previewSettings.lookAtDoorSide && (p.x = physicalMetrics.totalSystemSize.width - p.x), p.x = this.screenSpaceTransform.x + p.x * this.screenSpaceTransform.scaleFactor, p.y = physicalMetrics.totalSystemSize.height - p.y, p.y = this.screenSpaceTransform.y + p.y * this.screenSpaceTransform.scaleFactor
        }
    }
}]), siteApp.factory("previewHotSpotManager", ["domManager", "previewSettings", function(domManager, previewSettings) {
    return {
        hotspots: [],
        init: function() {},
        reset: function() {
            this.hotspots = [], previewSettings.hotspotMarkers = []
        },
        addHotSpot: function(x, y, width, height, onClick, markerType) {
            var hotspot = {
                x: x,
                y: y,
                width: width,
                height: height,
                onClick: onClick
            };
            this.hotspots.push(hotspot);
            var iconOverlaySize = 20;
            if (null != markerType) {
                var hotspotMarker = {
                    x: x + (width - iconOverlaySize) / 2,
                    y: y + (height - iconOverlaySize) / 2,
                    type: markerType
                };
                previewSettings.hotspotMarkers.push(hotspotMarker)
            }
        },
        handlePreviewContainerClick: function(x, y) {
            for (var i = 0; i < this.hotspots.length; i++) {
                var hotspot = this.hotspots[i];
                if (x >= hotspot.x && x <= hotspot.x + hotspot.width && y >= hotspot.y && y <= hotspot.y + hotspot.height) return void hotspot.onClick()
            }
        }
    }
}]), siteApp.factory("previewRenderer", ["domManager", "previewRasteriser", "previewHotSpotManager", "previewDoorsRenderer", "previewSettings", "staticData", "inputData", "calculatedData", "util", function(domManager, previewRasteriser, previewHotSpotManager, previewDoorsRenderer, previewSettings, staticData, inputData, calculatedData) {
    return {
        init: function() {},
        render: function() {
            console.log("Render");
            var previewContainerSize = (inputData.openingSize, domManager.getElementSize("preview-container")),
                renderTargetSize = {
                    width: previewContainerSize.width,
                    height: previewContainerSize.height,
                    padding: 50
                },
                hotspots = [],
                job = this.buildRasterJob(renderTargetSize, hotspots);
            previewRasteriser.executeRasterJob(job);
            for (var i = 0; i < hotspots.length; i++) {
                var hotspot = hotspots[i];
                previewHotSpotManager.addHotSpot(hotspot.x, hotspot.y, hotspot.width, hotspot.height, hotspot.onClick, hotspot.markerType)
            }
        },
        buildRasterJob: function(renderTargetSize, hotspots) {
            hotspots = hotspots || [];
            var wardrobeScreenSpaceTransform = this.buildWardrobeScreenSpaceTransform(renderTargetSize);
            previewHotSpotManager.reset();
            var job = new RasterJob(renderTargetSize.width, renderTargetSize.height);
            return job.clear(), this.drawBackground(job, renderTargetSize), previewDoorsRenderer.renderDoors(job, wardrobeScreenSpaceTransform, hotspots), this.drawAxes(job, wardrobeScreenSpaceTransform), job
        },
        drawBackground: function(job, renderTargetSize) {
            domManager.getElementSize("preview-container");
            job.fillRect(0, 0, renderTargetSize.width, renderTargetSize.height, "#f4f4f4")
        },
        drawAxes: function(job, wardrobeScreenSpaceTransform) {
            var totalSystemSize = calculatedData.physicalMetrics.totalSystemSize,
                labelLength = 80,
                barLength = totalSystemSize.width * wardrobeScreenSpaceTransform.scaleFactor / 2 - labelLength / 2;
            job.fillRect(wardrobeScreenSpaceTransform.x, wardrobeScreenSpaceTransform.y + totalSystemSize.height * wardrobeScreenSpaceTransform.scaleFactor + 10, barLength, 2, "#aaaaff"), job.fillText(wardrobeScreenSpaceTransform.x + totalSystemSize.width * wardrobeScreenSpaceTransform.scaleFactor / 2 - 28, wardrobeScreenSpaceTransform.y + totalSystemSize.height * wardrobeScreenSpaceTransform.scaleFactor + 16, totalSystemSize.width + "mm", "14px arial", "black", 0), job.fillRect(wardrobeScreenSpaceTransform.x + barLength + labelLength, wardrobeScreenSpaceTransform.y + totalSystemSize.height * wardrobeScreenSpaceTransform.scaleFactor + 10, barLength, 2, "#aaaaff"), barLength = totalSystemSize.height * wardrobeScreenSpaceTransform.scaleFactor / 2 - labelLength / 2, job.fillRect(wardrobeScreenSpaceTransform.x - 12, wardrobeScreenSpaceTransform.y, 2, barLength, "#aaaaff"), job.fillText(-(wardrobeScreenSpaceTransform.y + totalSystemSize.height * wardrobeScreenSpaceTransform.scaleFactor / 2 + 28), wardrobeScreenSpaceTransform.x - 6, totalSystemSize.height + "mm", "14px arial", "black", 2 * Math.PI * -.25), job.fillRect(wardrobeScreenSpaceTransform.x - 12, wardrobeScreenSpaceTransform.y + barLength + labelLength, 2, barLength, "#aaaaff")
        },
        buildWardrobeScreenSpaceTransform: function(renderTargetSize) {
            var totalSystemSize = calculatedData.physicalMetrics.totalSystemSize,
                maxScreenSpaceWardrobeWidth = renderTargetSize.width - renderTargetSize.padding,
                maxScreenSpaceWardrobeHeight = renderTargetSize.height - renderTargetSize.padding,
                scaleFactor = this.calculateWardrobeScreenSpaceTransformScaleFactor(maxScreenSpaceWardrobeWidth, maxScreenSpaceWardrobeHeight),
                wardrobeScreenSpaceOffsetX = (renderTargetSize.width - totalSystemSize.width * scaleFactor) / 2,
                wardrobeScreenSpaceOffsetY = (renderTargetSize.height - totalSystemSize.height * scaleFactor) / 2,
                transform = {
                    x: wardrobeScreenSpaceOffsetX,
                    y: wardrobeScreenSpaceOffsetY,
                    scaleFactor: scaleFactor
                };
            return transform
        },
        calculateWardrobeScreenSpaceTransformScaleFactor: function(maxScreenSpaceWardrobeWidth, maxScreenSpaceWardrobeHeight) {
            var totalSystemSize = calculatedData.physicalMetrics.totalSystemSize,
                scaleFactor = 1;
            return scaleFactor = maxScreenSpaceWardrobeWidth / maxScreenSpaceWardrobeHeight > totalSystemSize.width / totalSystemSize.height ? maxScreenSpaceWardrobeHeight / totalSystemSize.height : maxScreenSpaceWardrobeWidth / totalSystemSize.width, scaleFactor = Math.round(100 * scaleFactor) / 100
        }
    }
}]), siteApp.factory("previewSettings", [function() {
    return {
        enableEditMode: !1,
        lookAtDoorSide: "Front",
        hotspotMarkers: [],
        doorPanelClickHandler: null
    }
}]), siteApp.factory("priceCalculator", ["inputData", "calculatedData", "staticData", "price", "api", "util", function(inputData, calculatedData, staticData, price, api, util) {
    var recalculatePriceCheckSum = 0;
    return {
        recalculatePrice: function() {
            console.log("Recalculating price."), recalculatePriceCheckSum = Math.random();
            var trimmedCalculatedData = {
                physicalMetrics: calculatedData.physicalMetrics
            };
            api.calculatePriceForWardrobeConfiguration(staticData.version, inputData, trimmedCalculatedData, recalculatePriceCheckSum).then(function(result) {
                result.checksum == recalculatePriceCheckSum && util.copyObjectPropertiesToOtherObject(result.priceCalculation, price);

            })
        }
    }
}]), siteApp.factory("price", [function() {
    return {
        totalPriceIncVat: 0
    }
}]), siteApp.factory("renderMetricsCalculator", ["staticData", "inputData", "calculatedData", "doorPlacementPlanManager", "util", function(staticData, inputData, calculatedData, doorPlacementPlanManager, util) {
    return {
        calculateRenderMetrics: function() {
            {
                var doorPlacementPlan = doorPlacementPlanManager.identifySuitableDoorPlacementPlan(),
                    renderMetrics = {
                        individualDoorSize: {
                            width: 0,
                            height: 0
                        },
                        leftEndPanel: {
                            x: 0,
                            y: 0,
                            width: 0,
                            height: 0
                        },
                        rightEndPanel: {
                            x: 0,
                            y: 0,
                            width: 0,
                            height: 0
                        },
                        doorLiners: [],
                        doors: []
                    },
                    totalSystemSize = calculatedData.physicalMetrics.totalSystemSize,
                    aperturePosition = calculatedData.physicalMetrics.aperturePosition,
                    apertureSize = calculatedData.physicalMetrics.apertureSize;
                calculatedData.physicalMetrics.doorOverlapWidth
            }
            renderMetrics.individualDoorSize.width = calculatedData.physicalMetrics.requiredDoorWidth, renderMetrics.individualDoorSize.height = apertureSize.height, inputData.endPanelOptions.hasLeftEndPanel && (renderMetrics.leftEndPanel = {
                x: 0,
                y: totalSystemSize.height,
                width: staticData.endPanelThickness,
                height: totalSystemSize.height
            }), inputData.endPanelOptions.hasRightEndPanel && (renderMetrics.rightEndPanel = {
                x: totalSystemSize.width - staticData.endPanelThickness,
                y: totalSystemSize.height,
                width: staticData.endPanelThickness,
                height: totalSystemSize.height
            });
            for (var i = 0; i < inputData.doorLinerOptions.quantitiesByPlacementLocation.Top; i++) {
                var doorLinerMetrics = {
                    x: aperturePosition.x - inputData.doorLinerOptions.quantitiesByPlacementLocation.Left * staticData.doorLinerThickness,
                    y: aperturePosition.y + (inputData.doorLinerOptions.quantitiesByPlacementLocation.Top - i) * staticData.doorLinerThickness,
                    width: apertureSize.width + (inputData.doorLinerOptions.quantitiesByPlacementLocation.Left + inputData.doorLinerOptions.quantitiesByPlacementLocation.Right) * staticData.doorLinerThickness,
                    height: staticData.doorLinerThickness
                };
                renderMetrics.doorLiners.push(doorLinerMetrics)
            }
            for (var i = 0; i < inputData.doorLinerOptions.quantitiesByPlacementLocation.Bottom; i++) {
                var doorLinerMetrics = {
                    x: aperturePosition.x - inputData.doorLinerOptions.quantitiesByPlacementLocation.Left * staticData.doorLinerThickness,
                    y: (inputData.doorLinerOptions.quantitiesByPlacementLocation.Bottom - i) * staticData.doorLinerThickness,
                    width: apertureSize.width + (inputData.doorLinerOptions.quantitiesByPlacementLocation.Left + inputData.doorLinerOptions.quantitiesByPlacementLocation.Right) * staticData.doorLinerThickness,
                    height: staticData.doorLinerThickness
                };
                renderMetrics.doorLiners.push(doorLinerMetrics)
            }
            for (var i = 0; i < inputData.doorLinerOptions.quantitiesByPlacementLocation.Left; i++) {
                var doorLinerMetrics = {
                    x: aperturePosition.x - (inputData.doorLinerOptions.quantitiesByPlacementLocation.Left - i) * staticData.doorLinerThickness,
                    y: aperturePosition.y,
                    width: staticData.doorLinerThickness,
                    height: apertureSize.height
                };
                renderMetrics.doorLiners.push(doorLinerMetrics)
            }
            for (var i = 0; i < inputData.doorLinerOptions.quantitiesByPlacementLocation.Right; i++) {
                var doorLinerMetrics = {
                    x: aperturePosition.x + apertureSize.width + i * staticData.doorLinerThickness,
                    y: aperturePosition.y,
                    width: staticData.doorLinerThickness,
                    height: apertureSize.height
                };
                renderMetrics.doorLiners.push(doorLinerMetrics)
            }
            return this.resolveDoorMetrics(renderMetrics, doorPlacementPlan), renderMetrics
        },
        resolveDoorMetrics: function(renderMetrics, doorPlacementPlan) {
            for (var self = this, aperturePosition = calculatedData.physicalMetrics.aperturePosition, doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], renderPatternConstantOverridesByName = util.convertArrayToHashTable(doorRange.doorRenderPatternConstantOverrides, "name"), doorX = aperturePosition.x, doorIndex = 0; doorIndex < inputData.doorOptions.numberOfDoors; doorIndex++) {
                var doorSelection = inputData.doorSelections[doorIndex],
                    doorPlacement = doorPlacementPlan.doorPlacementsByDoorNumber[doorIndex];
                doorPlacement.leftOverlap && (doorX -= calculatedData.physicalMetrics.doorOverlapWidth);
                var doorMetrics = {
                        x: doorX,
                        y: aperturePosition.y,
                        width: renderMetrics.individualDoorSize.width,
                        height: renderMetrics.individualDoorSize.height,
                        panels: [],
                        frames: [],
                        handles: [],
                        trackNumber: doorPlacement.trackNumber
                    },
                    selectedDoorPanelConfiguration = staticData.doorPanelConfigurationsById[doorSelection.doorPanelConfigurationId],
                    doorRenderPattern = selectedDoorPanelConfiguration.details.doorRenderPattern,
                    renderPatternConstantsByName = util.convertArrayToHashTable(doorRenderPattern.constants, "name");
                util.copyObjectPropertiesToOtherObject(renderPatternConstantOverridesByName, renderPatternConstantsByName);
                for (var resolveRenderPatternValue = function(instructions) {
                        return self.resolveDoorPanelConfigurationRenderPatternValue(instructions, doorMetrics, renderPatternConstantsByName)
                    }, panelIndex = 0; panelIndex < doorRenderPattern.panelRects.length; panelIndex++) {
                    var panelRect = doorRenderPattern.panelRects[panelIndex],
                        panelMetrics = {
                            x: resolveRenderPatternValue(panelRect.x),
                            y: resolveRenderPatternValue(panelRect.y),
                            width: resolveRenderPatternValue(panelRect.width),
                            height: resolveRenderPatternValue(panelRect.height)
                        };
                    panelMetrics.x += doorMetrics.x, panelMetrics.y = doorMetrics.y - panelMetrics.y, doorMetrics.panels.push(panelMetrics)
                }
                for (var frameIndex = 0; frameIndex < doorRenderPattern.frameRects.length; frameIndex++) {
                    var frameRect = doorRenderPattern.frameRects[frameIndex],
                        frameMetrics = {
                            x: resolveRenderPatternValue(frameRect.x),
                            y: resolveRenderPatternValue(frameRect.y),
                            width: resolveRenderPatternValue(frameRect.width),
                            height: resolveRenderPatternValue(frameRect.height),
                            bevels: frameRect.bevels
                        };
                    frameMetrics.x += doorMetrics.x, frameMetrics.y = doorMetrics.y - frameMetrics.y, doorMetrics.frames.push(frameMetrics)
                }
                var applicableHandlePlan = this.findApplicableHandlePlan(doorIndex);
                if (null != applicableHandlePlan) {
                    var handlePlacementPlan = applicableHandlePlan.handlePlacementPlansByNumberOfDoors[inputData.doorOptions.numberOfDoors];
                    if (null != handlePlacementPlan) {
                        var handleLocation = handlePlacementPlan.handleLocationsByDoorIndex[doorIndex];
                        "MiddleHorizontalFrame" == applicableHandlePlan.handleType ? this.buildMidHorizontalFrameHandleMetrics(doorMetrics, handleLocation) : "MiddlePanel" == applicableHandlePlan.handleType && this.buildMiddlePanelHandleMetrics(doorMetrics, handleLocation)
                    }
                }
                renderMetrics.doors.push(doorMetrics), doorX += renderMetrics.individualDoorSize.width
            }
        },
        resolveDoorPanelConfigurationRenderPatternValue: function(instructions, doorMetrics, constantsByName) {
            for (var value = 0, variableValuesByName = {
                    DoorWidth: doorMetrics.width,
                    DoorHeight: doorMetrics.height
                }, getConstantValue = function(constantName) {
                    var constant = constantsByName[constantName];
                    return null != constant ? constant.value : (console.log("Door render plan constant not found: " + constantName), 0)
                }, i = 0; i < instructions.length; i++) {
                var instruction = instructions[i],
                    operationValue = 0;
                "AddVariable" == instruction.operation ? operationValue = variableValuesByName[instruction.variable] * instruction.scale : "SubtractVariable" == instruction.operation ? operationValue = -variableValuesByName[instruction.variable] * instruction.scale : "AddMillimeters" == instruction.operation ? operationValue = instruction.millimeters : "SubtractMillimeters" == instruction.operation ? operationValue = -instruction.millimeters : "AddConstant" == instruction.operation ? operationValue = getConstantValue(instruction.constantName) * instruction.scale : "SubtractConstant" == instruction.operation && (operationValue = -getConstantValue(instruction.constantName) * instruction.scale), value += operationValue
            }
            return value
        },
        buildMidHorizontalFrameHandleMetrics: function(doorMetrics, handleLocation) {
            var handleWidth = 100,
                handleHeight = 35;
            if ("Left" == handleLocation || "Both" == handleLocation) {
                var handleMetrics = {
                    x: 0,
                    y: doorMetrics.height / 2 - handleHeight / 2,
                    width: handleWidth,
                    height: handleHeight,
                    handleType: "FrameHandle"
                };
                handleMetrics.x += doorMetrics.x, handleMetrics.y = doorMetrics.y - handleMetrics.y, doorMetrics.handles.push(handleMetrics)
            }
            if ("Right" == handleLocation || "Both" == handleLocation) {
                var handleMetrics = {
                    x: doorMetrics.width - handleWidth,
                    y: doorMetrics.height / 2 - handleHeight / 2,
                    width: handleWidth,
                    height: handleHeight,
                    handleType: "FrameHandle"
                };
                handleMetrics.x += doorMetrics.x, handleMetrics.y = doorMetrics.y - handleMetrics.y, doorMetrics.handles.push(handleMetrics)
            }
        },
        buildMiddlePanelHandleMetrics: function(doorMetrics, handleLocation) {
            var middlePanelIndex = Math.ceil((doorMetrics.panels.length - 1) / 2),
                middlePanelMetrics = doorMetrics.panels[middlePanelIndex],
                handleWidth = 200,
                handleHeight = 50,
                handleMargin = 35;
            if ("Left" == handleLocation || "Both" == handleLocation) {
                var handleMetrics = {
                    x: middlePanelMetrics.x + handleMargin,
                    y: middlePanelMetrics.y - middlePanelMetrics.height / 2 + handleHeight / 2,
                    width: handleWidth,
                    height: handleHeight,
                    handleType: "PanelHandle"
                };
                doorMetrics.handles.push(handleMetrics)
            }
            if ("Right" == handleLocation || "Both" == handleLocation) {
                var handleMetrics = {
                    x: middlePanelMetrics.x + middlePanelMetrics.width - handleWidth - handleMargin,
                    y: middlePanelMetrics.y - middlePanelMetrics.height / 2 + handleHeight / 2,
                    width: handleWidth,
                    height: handleHeight,
                    handleType: "PanelHandle"
                };
                doorMetrics.handles.push(handleMetrics)
            }
        },
        findApplicableHandlePlan: function(doorIndex) {
            for (var applicableHandlePlan = null, doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId], doorSelections = inputData.doorSelections[doorIndex], i = 0; i < doorRange.handlePlans.length; i++) {
                var handlePlan = doorRange.handlePlans[i],
                    handlePlanIsApplicable = !1;
                if ((handlePlan.appliesToAllDoorPanelConfigurations || -1 != util.inArray(handlePlan.applicableDoorPanelConfigurationIds, doorSelections.doorPanelConfigurationId)) && (handlePlanIsApplicable = !0), handlePlanIsApplicable) {
                    applicableHandlePlan = handlePlan;
                    break
                }
            }
            return applicableHandlePlan
        }
    }
}]), siteApp.factory("renderingImageManager", ["staticData", function(staticData) {
    return {
        imagesById: null,
        init: function(callback) {
            this.imagesById = {};
            for (var numberOfImagesLoaded = 0, i = 0; i < staticData.renderingImageDefinitions.length; i++) {
                var renderingImageDefinition = staticData.renderingImageDefinitions[i],
                    image = document.createElement("img");
                this.imagesById[renderingImageDefinition.id] = image, image.onload = function() {
                    console.log("Image loaded: " + this.src), numberOfImagesLoaded++, numberOfImagesLoaded == staticData.renderingImageDefinitions.length && (console.log("All images loaded."), callback())
                }, image.src = renderingImageDefinition.url
            }
        },
        findImage: function(imageId) {
            return this.imagesById[imageId]
        }
    }
}]), siteApp.factory("settings", [function() {
    return {}
}]), siteApp.factory("staticData", ["dataUrlBuilder", "scl", "api", "util", function(dataUrlBuilder, scl, api, util) {
    return console.log("Instantiated static data."), {
        load: function(options) {
            var self = this,
                id = util.getQueryStringParameter("id"),
                source = util.getQueryStringParameter("source") || "NewConfiguration",
                debugCmd = util.getQueryStringParameter("debugCmd");
            api.loadConfiguratorStaticData(id, options.mode, source, debugCmd).then(function(response) {
                for (var trace = response.trace, i = 0; i < trace.errors.length; i++) {
                    var error = trace.errors[i];
                    console.error(error)
                }
                for (var i = 0; i < trace.warnings.length; i++) {
                    var warning = trace.warnings[i];
                    console.error(warning)
                }
                util.copyObjectPropertiesToOtherObject(response, self), self.buildAdditionalData(), 0 == util.countHashTableKeys(self.doorRangesById) ? options.onNoDoorRangesAvailable() : trace.errors.length > 0 ? options.onFailed(trace.errors) : options.onLoaded()
            })
        },
        buildAdditionalData: function() {
            this.version = "1.1", this.materialTypes = [{
                type: "Wood",
                displayName: "Wood"
            }, {
                type: "Glass",
                displayName: "Glass"
            }, {
                type: "ToughenedGlass",
                displayName: "Toughened Glass"
            }], this.materialIds = util.getObjectPropertyNamesAsArray(this.materialsById), this.materials = util.getObjectPropertyValuesAsArray(this.materialsById), this.doorPanelIds = util.getObjectPropertyNamesAsArray(this.doorPanelsById), this.doorPanels = util.getObjectPropertyValuesAsArray(this.doorPanelsById), this.doorPanelIdsByMaterialType = this.buildDoorPanelIdsByMaterialTypeLookup(this.doorPanelIds), this.doorRanges = util.getObjectPropertyValuesAsArray(this.doorRangesById), this.doorPanelConfigurationIds = util.getObjectPropertyNamesAsArray(this.doorPanelConfigurationsById), this.doorPanelConfigurations = util.getObjectPropertyValuesAsArray(this.doorPanelConfigurationsById);
            var metalMaterialTypes = ["Steel", "Aluminium"];
            this.metalMaterialIds = [];
            for (var i = 0; i < this.materials.length; i++) {
                var material = this.materials[i]; - 1 != util.inArray(metalMaterialTypes, material.details.materialType) && this.metalMaterialIds.push(material.id)
            }
            for (var i = 0; i < this.doorRanges.length; i++) {
                var doorRange = this.doorRanges[i];
                doorRange.allowedFrameMaterialIdsLookup = util.convertScalarArrayToHashTable(doorRange.allowedFrameMaterialIds), doorRange.allowedDoorPanelConfigurationIdsLookup = util.convertScalarArrayToHashTable(doorRange.allowedDoorPanelConfigurationIds), doorRange.allowedDoorPanelIdsLookup = util.convertScalarArrayToHashTable(doorRange.allowedDoorPanelIds), doorRange.allowedTrackSetTypesLookup = util.convertScalarArrayToHashTable(doorRange.allowedTrackSetTypes), doorRange.allowedTrackSetMountingTypesLookup = util.convertScalarArrayToHashTable(doorRange.allowedTrackSetMountingTypes)
            }
            this.interiorProductRanges = util.getObjectPropertyValuesAsArray(this.interiorProductRangesById), this.interiors = util.getObjectPropertyValuesAsArray(this.interiorsByProductId), this.endPanelThickness = 18, this.doorLinerThickness = 18, this.defaultOpeningSizeRules = {
                minimumOpeningSize: {
                    width: 1e3,
                    height: 1e3
                },
                maximumOpeningSize: {
                    width: 4500,
                    height: 3e3
                }
            }, this.defaultDoorSizeRules = {
                minDoorWidth: 700,
                maxDoorWidth: 1400
            }, this.doorSides = ["Front", "Back"], this.doorOverlappingModes = [{
                mode: "MeetInTheMiddle",
                displayName: scl.load("configurator-content/field-labels/door-overlapping-mode-meet-in-the-middle")
            }, {
                mode: "Interleaving",
                displayName: scl.load("configurator-content/field-labels/door-overlapping-mode-interleaving")
            }], this.endPanelMaterialSelectionModes = [{
                mode: "Automatic",
                displayName: scl.load("configurator-content/field-labels/end-panel-material-selection-mode-automatic")
            }, {
                mode: "Manual",
                displayName: scl.load("configurator-content/field-labels/end-panel-material-selection-mode-manual")
            }], this.doorLinerSelectionModes = [{
                mode: "Automatic",
                displayName: scl.load("configurator-content/field-labels/door-liner-selection-mode-automatic"),
                requiresPermission: !1
            }, {
                mode: "AutomaticWithManualMaterial",
                displayName: scl.load("configurator-content/field-labels/door-liner-selection-mode-automatic-with-manual-material"),
                requiresPermission: !1
            }, {
                mode: "Manual",
                displayName: scl.load("configurator-content/field-labels/door-liner-selection-mode-manual"),
                requiresPermission: !0
            }, {
                mode: "NoLiners",
                displayName: scl.load("configurator-content/field-labels/door-liner-selection-mode-no-liners"),
                requiresPermission: !1
            }], this.doorLinerPlacementLocations = [{
                placementLocation: "Top",
                displayName: "Top",
                orientation: "Horizontal"
            }, {
                placementLocation: "Bottom",
                displayName: "Bottom",
                orientation: "Horizontal"
            }, {
                placementLocation: "Left",
                displayName: "Left",
                orientation: "Vertical"
            }, {
                placementLocation: "Right",
                displayName: "Right",
                orientation: "Vertical"
            }], this.trackSetTypeSelectionModes = [{
                mode: "Automatic",
                displayName: scl.load("configurator-content/field-labels/track-set-type-selection-mode-automatic")
            }, {
                mode: "Manual",
                displayName: scl.load("configurator-content/field-labels/track-set-type-selection-mode-manual")
            }], this.trackSetMountingTypes = [{
                type: "CeilingMounted",
                displayName: scl.load("configurator-content/field-labels/track-set-mounting-type-ceiling-mounted")
            }, {
                type: "WallMounted",
                displayName: scl.load("configurator-content/field-labels/track-set-mounting-type-wall-mounted")
            }], this.textureImageConfig = {
                worldSpaceSize: {
                    width: 3e3,
                    height: 3e3
                },
                screenSpaceSize: {
                    width: 800,
                    height: 800
                }
            }, this.interiorImagesByProductId = {};
            for (var i = 0; i < this.interiors.length; i++) {
                var interior = this.interiors[i],
                    imageUrl = dataUrlBuilder.buildDataUrl("/product-media/ProductImage", {
                        productId: interior.productId,
                        mw: 130,
                        mh: 135
                    });
                this.interiorImagesByProductId[interior.productId] = imageUrl
            }
            this.doorRangePickerItemCssClass = "col-sm-4";
            var numberOfDoorRanges = util.countHashTableKeys(this.doorRangesById);
            numberOfDoorRanges % 4 == 0 ? this.doorRangePickerItemCssClass = "col-sm-3" : numberOfDoorRanges % 3 == 0 ? this.doorRangePickerItemCssClass = "col-sm-4" : numberOfDoorRanges % 4 == 3 && (this.doorRangePickerItemCssClass = "col-sm-3")
        },
        buildDoorPanelIdsByMaterialTypeLookup: function(doorPanelIds) {
            for (var doorPanelIdsByMaterialType = {}, i = 0; i < this.materialTypes.length; i++) {
                var materialType = this.materialTypes[i];
                doorPanelIdsByMaterialType[materialType.type] = [];
                for (var j = 0; j < doorPanelIds.length; j++) {
                    var doorPanelId = doorPanelIds[j],
                        doorPanel = this.doorPanelsById[doorPanelId],
                        material = this.materialsById[doorPanel.details.materialId];
                    material.details.materialType == materialType.type && doorPanelIdsByMaterialType[materialType.type].push(doorPanel.id)
                }
            }
            return doorPanelIdsByMaterialType
        }
    }
}]), siteApp.factory("submissionDetails", [function() {
    return console.log("Instantiated submission details."), {
        sourceSavedWardrobeConfigurationId: null
    }
}]), siteApp.factory("submitter", ["$http", "$window", "dataUrlBuilder", "api", "staticData", "inputData", "calculatedData", "customerDetails", "installationDetails", "submissionDetails", "previewRenderer", function($http, $window, dataUrlBuilder, api, staticData, inputData, calculatedData, customerDetails, installationDetails, submissionDetails, previewRenderer) {
    return {
        upsertWardrobeConfiguratorCustomer: function() {
            api.upsertWardrobeConfiguratorCustomer(staticData.version, customerDetails.emailAddress, customerDetails.marketingOptIn || !1)
        },
        saveForCustomer: function(callback) {
            api.saveWardrobeConfigurationForCustomer(staticData.version, submissionDetails.sourceSavedWardrobeConfigurationId, inputData, this.buildTrimmedCalculatedData(), customerDetails).then(function(result) {
                submissionDetails.sourceSavedWardrobeConfigurationId = result.savedWardrobeConfigurationId, console.log("Wardrobe configuration saved."), null != callback && callback()
            })
        },
        saveForSalesUser: function(sendEmailToCustomer, callback) {
            api.saveWardrobeConfigurationForSalesUser(staticData.version, submissionDetails.sourceSavedWardrobeConfigurationId, inputData, this.buildTrimmedCalculatedData(), customerDetails, sendEmailToCustomer).then(function(result) {
                submissionDetails.sourceSavedWardrobeConfigurationId = result.savedWardrobeConfigurationId, console.log("Wardrobe configuration saved."), null != callback && callback()
            })
        },
        generateCutSheet: function() {
            var form = $("#cut-sheet-generator-form");
            form.attr("action", dataUrlBuilder.buildDataUrl("/configurator-api/GenerateCutSheetForInProgressWardrobeConfiguration")), form.find('input[name="wardrobeConfigurationId"]').val(staticData.wardrobeConfigurationId), form.find('input[name="inputDataJson"]').val(JSON.stringify(inputData)), form.find('input[name="calculatedDataJson"]').val(JSON.stringify(this.buildTrimmedCalculatedData())), form.find('input[name="compactRasterJob"]').val(this.buildRasterJob().buildCompactString()), form.submit()
        },
        generateRouteLines: function() {
            var form = $("#route-lines-generator-form");
            form.attr("action", dataUrlBuilder.buildDataUrl("/configurator-api/GenerateRouteLinesForInProgressWardrobeConfiguration")), form.find('input[name="wardrobeConfigurationId"]').val(staticData.wardrobeConfigurationId), form.find('input[name="inputDataJson"]').val(JSON.stringify(inputData)), form.find('input[name="calculatedDataJson"]').val(JSON.stringify(this.buildTrimmedCalculatedData())), form.submit()
        },
        generatePrintableSummary: function() {
            var form = $("#printable-summary-generator-form");
            form.attr("action", dataUrlBuilder.buildDataUrl("/configurator-api/GeneratePrintableSummaryForInProgressWardrobeConfiguration")), form.find('input[name="inputDataJson"]').val(JSON.stringify(inputData)), form.find('input[name="compactRasterJob"]').val(this.buildRasterJob().buildCompactString()), form.submit()
        },
        submitToBasket: function(callback) {
            api.addWardrobeConfigurationToBasket(staticData.version, staticData.wardrobeConfigurationId, submissionDetails.sourceSavedWardrobeConfigurationId, inputData, this.buildTrimmedCalculatedData(), this.buildRasterJob().buildCompactString(), customerDetails, installationDetails).then(function(result) {
                null != callback && callback(result)
            })
        },
        buildRasterJob: function() {
            var padding = 50,
                desiredHeight = 600,
                aspectRatio = (inputData.openingSize.width + padding) / (inputData.openingSize.height + padding),
                renderTargetSize = {
                    width: Math.ceil(desiredHeight * aspectRatio),
                    height: desiredHeight,
                    padding: padding
                },
                rasterJob = previewRenderer.buildRasterJob(renderTargetSize);
            return rasterJob
        },
        buildTrimmedCalculatedData: function() {
            var trimmedCalculatedData = {
                physicalMetrics: calculatedData.physicalMetrics
            };
            return trimmedCalculatedData
        }
    }
}]), siteApp.factory("tabData", ["staticData", "inputData", "util", function(staticData, inputData, util) {
    for (var tabs = [{
            id: "dimensions",
            name: "Dimensions",
            enabled: function() {
                return !0
            }
        }, {
            id: "range",
            name: "Range",
            enabled: function() {
                return !0
            }
        }, {
            id: "doors-and-frames",
            name: "Doors & Frames",
            enabled: function() {
                return !0
            }
        }, {
            id: "door-panels",
            name: "Door Panels",
            enabled: function() {
                return !0
            }
        }, {
            id: "liners",
            name: "Liners",
            enabled: function() {
                var configurationOnlyContainsOneDoor = 1 == inputData.doorOptions.numberOfDoors;
                return "Wardrobe" == staticData.mode && staticData.configuratorSettings.enableLiners && !configurationOnlyContainsOneDoor
            }
        }, {
            id: "end-panels",
            name: "End Panels",
            enabled: function() {
                return "Wardrobe" == staticData.mode
            }
        }, {
            id: "track-sets",
            name: "Track Sets",
            enabled: function() {
                var userHasAtLeastOneTrackSetPermission = staticData.permissions.canAddExtraTrackSets || staticData.permissions.canOverrideTrackSetType || staticData.permissions.canOverrideTrackSetWidth,
                    doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId],
                    selectedDoorRangeHasMultipleAllowedTrackSetMountingTypes = doorRange.allowedTrackSetMountingTypes.length > 1,
                    configurationOnlyContainsOneDoor = 1 == inputData.doorOptions.numberOfDoors;
                return "RoomDivider" == staticData.mode || userHasAtLeastOneTrackSetPermission || selectedDoorRangeHasMultipleAllowedTrackSetMountingTypes || configurationOnlyContainsOneDoor
            }
        }, {
            id: "interior",
            name: "Interior",
            enabled: function() {
                var atLeastOneInteriorProductRangeIsAvailable = staticData.interiors.length > 0;
                return "Wardrobe" == staticData.mode && atLeastOneInteriorProductRangeIsAvailable
            }
        }, {
            id: "installation",
            name: "Installation",
            enabled: function() {
                var enabled = staticData.configuratorSettings.enableInstallation;
                return staticData.isLoggedInAsSalesUser && staticData.configuratorSettings.hideInstallationForSalesUsers && (enabled = !1), enabled
            }
        }, {
            id: "review",
            name: "Review",
            enabled: function() {
                return !0
            }
        }], i = 0; i < tabs.length; i++) {
        var tab = tabs[i];
        tab.previousTab = i > 0 ? tabs[i - 1] : null, tab.nextTab = i < tabs.length - 1 ? tabs[i + 1] : null
    }
    return {
        tabs: tabs,
        tabsById: util.convertArrayToHashTable(tabs, "id")
    }
}]), siteApp.factory("validator", ["staticData", "inputData", "calculatedData", "installationDetails", "openingSizeRulesHelper", "doorPanelPlacementValidityBuilder", "doorPanelConfigurationSelectionChecker", "settings", "util", function(staticData, inputData, calculatedData, installationDetails, openingSizeRulesHelper, doorPanelPlacementValidityBuilder, doorPanelConfigurationSelectionChecker, settings, util) {
    return {
        validateInputData: function() {
            var openingSizeRules = openingSizeRulesHelper.getOpeningSizeRules(),
                doorQuantityRules = calculatedData.doorQuantityRules,
                doorRange = staticData.doorRangesById[inputData.doorOptions.doorRangeId],
                hasEndPanels = inputData.endPanelOptions.hasLeftEndPanel || inputData.endPanelOptions.hasRightEndPanel,
                linerQuantities = inputData.doorLinerOptions.quantitiesByPlacementLocation,
                totalLiners = linerQuantities.Top + linerQuantities.Bottom + linerQuantities.Left + linerQuantities.Right,
                hasLiners = totalLiners > 0,
                doorPanelPlacementValidity = doorPanelPlacementValidityBuilder.buildDoorPanelPlacementValidity(),
                doorIndexesWithInvalidDoorPanelConfigurationSelection = doorPanelConfigurationSelectionChecker.identifyDoorsWithInvalidDoorPanelConfigurationSelection(),
                openingSizeWidth = inputData.openingSize.width || 0,
                openingSizeHeight = inputData.openingSize.height || 0,
                trackSetWidthOverride = inputData.trackSetOptions.trackSetWidthOverride || 0,
                validity = {
                    errors: {
                        openingTooThin: openingSizeWidth < openingSizeRules.minimumOpeningSize.width,
                        openingTooWide: openingSizeWidth > openingSizeRules.maximumOpeningSize.width,
                        openingTooShort: openingSizeHeight < openingSizeRules.minimumOpeningSize.height,
                        openingTooTall: openingSizeHeight > openingSizeRules.maximumOpeningSize.height,
                        tooFewDoors: inputData.doorOptions.numberOfDoors < doorQuantityRules.minNumberOfDoors,
                        tooManyDoors: inputData.doorOptions.numberOfDoors > doorQuantityRules.maxNumberOfDoors,
                        doorFrameMaterialInvalid: inputData.validationSettings.validateFrameMaterial && 1 != doorRange.allowedFrameMaterialIdsLookup[inputData.doorOptions.doorFrameMaterialId],
                        endPanelMaterialInvalid: hasEndPanels && -1 == util.inArray(doorRange.allowedEndPanelMaterialIds, inputData.endPanelOptions.materialId),
                        linerMaterialInvalid: hasLiners && -1 == util.inArray(doorRange.allowedLinerMaterialIds, inputData.doorLinerOptions.materialId),
                        oneOrMoreDoorsHaveInvalidDoorPanelConfigurationSelection: inputData.validationSettings.validateDoorPanelConfigurations && doorIndexesWithInvalidDoorPanelConfigurationSelection.length > 0,
                        linersRequiredForInstallation: "Wardrobe" == staticData.mode && settings.linersAreRequiredForInstallation && "Yes" == installationDetails.optInChoice && inputData.numberOfDoors > 1 && !hasLiners,
                        doorsTooWideForInstallation: "Yes" == installationDetails.optInChoice && calculatedData.physicalMetrics.doorsAreTooWideForInstallation,
                        trackSetWidthOverrideInvalid: inputData.trackSetOptions.overrideTrackSetWidth && 0 >= trackSetWidthOverride,
                        numberOfAdditionalTrackSetsInvalid: null == inputData.trackSetOptions.numberOfAdditionalSingleTrackSets || null == inputData.trackSetOptions.numberOfAdditionalDoubleTrackSets || null == inputData.trackSetOptions.numberOfAdditionalTripleTrackSets,
                        numberOfSoftClosesOverrideInvalid: inputData.trackSetOptions.overrideNumberOfSoftCloses && (null == inputData.trackSetOptions.numberOfSoftClosesOverride || inputData.trackSetOptions.numberOfSoftClosesOverride < 0)
                    },
                    warnings: {
                        oneOrMoreFrontSidePanelSelectionsAreInvalid: doorPanelPlacementValidity.validityHintsByDoorSide.Front.oneOrMorePanelSelectionsAreInvalid,
                        oneOrMoreFrontSidePanelSelectionsAreMissing: doorPanelPlacementValidity.validityHintsByDoorSide.Front.oneOrMorePanelSelectionsAreMissing,
                        oneOrMoreBackSidePanelSelectionsAreInvalid: doorPanelPlacementValidity.validityHintsByDoorSide.Back.oneOrMorePanelSelectionsAreInvalid,
                        oneOrMoreBackSidePanelSelectionsAreMissing: doorPanelPlacementValidity.validityHintsByDoorSide.Back.oneOrMorePanelSelectionsAreMissing
                    },
                    misplacedDoorPanelsLookup: doorPanelPlacementValidity.misplacedDoorPanelsLookup,
                    doorIndexesWithInvalidDoorPanelConfigurationSelection: doorIndexesWithInvalidDoorPanelConfigurationSelection
                };
            for (var propertyName in validity.errors)
                if (validity.errors[propertyName]) {
                    validity.hasErrors = !0;
                    break
                } for (var propertyName in validity.warnings)
                if (validity.warnings[propertyName]) {
                    validity.hasWarnings = !0;
                    break
                } return validity.inputDataIsInvalid = validity.hasErrors || validity.hasWarnings, validity
        }
    }
}]);